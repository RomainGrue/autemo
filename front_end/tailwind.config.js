module.exports = {
  content: [
    "src/components/**/*.{html,js,ts,tsx}",
    "src/pages/**/*.{html,js,ts,tsx}",
    "src/utils/**/*.{html,js,ts,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      'default': '#FFFFFF',
      'textarea': '#E5E5E5',
      'analysis': '#F178B6',
      'interface-button': '#5D5FEF',
    }
  },
  plugins: [],
};
