import { Switch, Route } from "react-router-dom";
import React from "react";
import "./assets/styles/App.css";
import "./assets/styles/tailwind.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./pages/Home/Home";
import { ReactNotifications } from "react-notifications-component";

function App() {
  return (
    <div>
      <Switch>
        <Route path="/">
          <ReactNotifications />
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
