import React, { SyntheticEvent } from "react";
import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Fera from "./Fera";
import { FERA, Mode } from "../../utils/interfaces";

Enzyme.configure({ adapter: new Adapter() });

describe("Reasons#Fera component", () => {
  const data: FERA = {
    participants: [
      {
        qualificateur: [
          {
            debut: 485,
            fin: 487,
          },
        ],
        role: "Narrateur",
      },
    ],
    phrases: [
      {
        emotion: 0,
        raison: [
          {
            id: 4,
            debut: 591,
            fin: 608,
          },
        ],
        debut: 0,
        fin: 131,
      },
      {
        emotion: 0,
        raison: [],
        debut: 132,
        fin: 181,
      },
      {
        emotion: 0,
        raison: [],
        debut: 182,
        fin: 268,
      },
      {
        emotion: 0,
        raison: [],
        debut: 269,
        fin: 381,
      },
      {
        emotion: 0,
        raison: [],
        debut: 382,
        fin: 425,
      },
      {
        emotion: 0,
        raison: [],
        debut: 426,
        fin: 617,
      },
      {
        emotion: 4,
        raison: [],
        debut: 618,
        fin: 746,
      },
      {
        emotion: 4,
        raison: [],
        debut: 747,
        fin: 837,
      },
      {
        emotion: 1,
        raison: [],
        debut: 838,
        fin: 866,
      },
      {
        emotion: 3,
        raison: [],
        debut: 867,
        fin: 1006,
      },
      {
        emotion: 0,
        raison: [],
        debut: 1007,
        fin: 1084,
      },
      {
        emotion: 4,
        raison: [],
        debut: 1085,
        fin: 1271,
      },
      {
        emotion: 3,
        raison: [],
        debut: 1272,
        fin: 1516,
      },
      {
        emotion: 4,
        raison: [],
        debut: 1517,
        fin: 1685,
      },
    ],
    faitsInterieurs: [
      {
        id: 7,
        debut: 747,
        fin: 792,
      },
    ],
    faitsExterieurs: [
      {
        id: 8,
        debut: 58,
        fin: 130,
      },
    ],
    actionsInterieurs: [
      {
        id: 2,
        text: "action interieure",
      },
    ],
    actionsExterieurs: [
      {
        id: 3,
        text: "action 1",
      },
      {
        id: 4,
        text: "action 2",
      },
    ],
    error: null,
    titre: "L'histoire de phillipe et alain",
    scene:
      "Après trois semaines d’épreuves écrites du concours CPGE, nous rédigions nos dernières copies pour l’épreuve de français des Mines.\nIl s’agissait de la dernière épreuve du concours.\nNous étions plus de 2000 étudiants, réunis dans le parc des expositions de Versailles.\nJ’étais placé au milieu de ce hall et j’étais entouré de 6 autres étudiants dont un ami sur ma diagonale droite.\nNous étions tous concentré sur notre copie.\nA quelques minutes de la fin, ma rédaction étant terminée, je lève le regard pour vérifier l’heure sur une horloge numérique rouge située devant la rangée d’une vingtaine de table devant moi.\nPuis, quelques dizaines de secondes après, je regarde les autres élèves composer et croise le regard de mon ami qui me souriait.\nJe ressentais alors d’abord une décontraction qui passe par un relâchement de mes muscles.\nJ’étais soudain moins tendu.\nEnsuite, j’ai senti des palpitations dans le ventre puis j’avais l’impression que mon visage s’illuminait avec presque des larmes aux yeux.\nPendant ce court moment, j’ai d’abord compris que mes concours étaient finis.\nJe visualisais et imaginais alors des explosions de joie une fois l’épreuve terminée, j’étais convaincu qu’il ne s’agissait que d’une excitation avant une réelle émotion de joie à venir.\nJ’ai ensuite pensé qu’il s’agissait des derniers moments que je passais dans cet impressionnant hall rempli d’élève et j’ai donc volontairement pris le temps de regarder autour de moi en espérant imprégner ma mémoire du souvenir de cette image.\nEnfin, en croisant le regard de mon ami, j’avais le sentiment que nous ressentions exactement la même émotion et qu’un sourir permettait de nous partager cette émotion.\n\n",
  };

  // eslint-disable-next-line
  const stringStringCB = jest.fn((path: string, emotion: string) => {});
  const stringNumCB = jest.fn((path: string, emotion: number) => {});
  const removeCallback = jest.fn((i: number) => {});
  const addActionCallback = jest.fn((p: string) => {});
  const zeroArityCallback = jest.fn(() => {});
  const syntheticEventCB = jest.fn((event: SyntheticEvent) => {});

  const wrapper = Enzyme.mount(
    <Fera
      removeActionExterieurCallBack={removeCallback}
      removeActionInterieurCallBack={removeCallback}
      addActionCallBack={addActionCallback}
      removeParticipant={removeCallback}
      addParticipantCallBack={zeroArityCallback}
      saveAnnotationsCallback={syntheticEventCB}
      annotationCallBack={stringStringCB}
      emotionCallback={stringNumCB}
      roleCallback={stringStringCB}
      id={"42"}
      data={data}
      mode={Mode.EDIT}
    />
  );

  /* rendering tests */
  it("Actions#Should render the correct amount of actions", () => {
    expect(wrapper.find("textarea.action-text").length).toEqual(3);
  });

  it("Reasons#Should render the correct amount of reasons", () => {
    expect(wrapper.find("p.reason-text").length).toEqual(1);
  });

  it("Roles#Should render the correct amount of floating labels", () => {
    const emotionBtns = wrapper.find("FloatingLabel");
    expect(emotionBtns.length).toEqual(1);
  });

  it("Reasons#Should render the correct amount of toggle buttons", () => {
    const phraseLists = wrapper.find("li.phrase-list");
    expect(phraseLists.length).toEqual(14);
    const emotionBtns = wrapper.find("button#dropdown-basic-button");
    expect(emotionBtns.length).toEqual(14);
    emotionBtns.at(0).simulate("click");
    //console.log(wrapper.update().debug())
    wrapper.find("DropdownItem").at(0).simulate("click");
    expect(stringNumCB.mock.calls[0][0]).toBe("phrases[0].emotion");
    expect(stringNumCB.mock.calls[0][1]).toBe(0);
  });

  /* simulate tests */
  it("Actions#remove action", () => {
    wrapper
      .findWhere((node) => node.key() === "remove-internal-action-btn-0")
      .simulate("click");
    expect(removeCallback.mock.calls.length).toBe(1);
  });

  it("Actions#add action", () => {
    wrapper
      .findWhere((node) => node.key() === "add-external-action")
      .simulate("click");
    expect(addActionCallback.mock.calls.length).toBe(1);
  });

  it("Participants#remove participants", () => {
    wrapper
      .findWhere((node) => node.key() === "participant-removal-btn-0")
      .simulate("click");
    expect(removeCallback.mock.calls.length).toBe(1);
  });

  it("Reasons#Should use callback with the right reasons path", () => {
    wrapper
      .findWhere((node) => node.key() === "reason-button-0")
      .simulate("click");
    expect(stringStringCB.mock.calls[0][0]).toBe("phrases[0].raison");
    expect(stringStringCB.mock.calls[0][1]).toBe("l_ReasonNumber 1");
  });
});
