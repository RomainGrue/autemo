import { withTranslation, WithTranslation } from "react-i18next";
import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import "../../assets/styles/Fera.css";
import {
  FERA,
  Mode,
  Emotions,
  Annotable,
  Qualificateur,
  Phrase,
  Participant,
  Action,
} from "../../utils/interfaces";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import { FloatingLabel, Form } from "react-bootstrap";
import { TFunction } from "i18next";

interface IProps extends WithTranslation {
  removeActionExterieurCallBack: (index: number) => void;
  removeActionInterieurCallBack: (index: number) => void;
  addParticipantCallBack: () => void;
  addActionCallBack: (path: string) => void;
  annotationCallBack: (path: string, tag: string) => void;
  saveAnnotationsCallback: (event: SyntheticEvent) => void;
  emotionCallback: (path: string, emotion: number) => void;
  roleCallback: (path: string, role: string) => void;
  removeParticipant: (index: number) => void;
  id: string;
  data: FERA;
  mode: Mode;
}

/**
 * Display an annotation Button.
 * The keyName must be unique in order to highlight only this button
 * @param props
 * @constructor
 */
function AnnotationButton(props: {
  i18nBtnName: string;
  onClick: () => void;
  isClicked: string;
  handleSetIsClicked: (key: string) => void;
  keyName: string;
  className: string;
  t: TFunction;
}) {
  const { onClick, isClicked, handleSetIsClicked, keyName, className, t } =
    props;

  /**
   * When click on the button, call onClick and setIsClicked
   */
  const handleClick = () => {
    onClick();
    handleSetIsClicked(keyName);
  };

  return (
    <button
      key={keyName}
      className={`${className} ${isClicked == keyName ? "bg-gray-400" : ""}`}
      onClick={handleClick}
    >
      {t(props.i18nBtnName)}
    </button>
  );
}

/**
 * Display the external or internal facts
 *
 * @param props
 * @constructor
 */
function GetFacts(props: {
  t: TFunction;
  factName: string;
  scene: string;
  annotationButtonClass: string;
  facts: Annotable[];
  onClick: () => void;
  isClicked: string;
  setIsClicked: (key: string) => void;
}) {
  const {
    t,
    annotationButtonClass,
    facts,
    onClick,
    scene,
    factName,
    isClicked,
    setIsClicked,
  } = props;
  const factClass = "flex flex-column w-full";
  //const factTitleClass = "text-uppercase text-sm w-1/2";

  return (
    <div className={factClass}>
      <div className={"flex flex-row items-center flex-wrap"}>
        <AnnotationButton
          i18nBtnName={factName}
          onClick={onClick}
          isClicked={isClicked}
          handleSetIsClicked={setIsClicked}
          keyName={factName}
          className={annotationButtonClass}
          t={t}
        />
      </div>

      <hr className={"w-4/5"} />
      <ul>
        {facts.map((f: Annotable, index: number) => {
          return (
            <li key={index} className="fera-list">
              <div className="fera-list-clickable">
                <p>{scene.slice(f.debut, f.fin)}</p>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

function Fera(props: IProps) {
  const { t, mode } = props;
  const [isClicked, setIsClicked] = useState("");
  const [emotionSelected, setEmotionSelected] = useState(5); //all emotions selected
  function selectEmotion(event: { target: { value: string } }) {
    const emotionSelected: number = parseInt(event.target.value);
    setEmotionSelected(emotionSelected);
  }
  const feraVisibility = mode === Mode.NEW_ANALYSIS ? "hide" : "block";
  const hide = mode === Mode.EDIT ? "" : "hide";

  // All classes name
  const subTitleClass = "text-uppercase text-sm border-b border-gray-200";
  const seuilClass = "text-sm border-b pl-2";
  const titleClass = "font-bold text-center uppercase text-lg pb-3";
  const emotionRaisonClass = "text-sm";
  const phraseClass = "text-base w-4/5";
  const basicButtonClass = `${hide} text-gray-700 font-semibold hover:bg-gray-300 border border-blue-500 hover:border-transparent rounded`;
  const annotationButtonClass = `${basicButtonClass} py-2 px-4`;
  const qualifiersButtonClass = `${basicButtonClass} py-3 px-4 mb-2`;
  const classNameActionsTextArea = "flex w-full border-1 m-2 action-text";

  /**
   * If the current id clicked is the same as the one in isClicked,
   * set isClicked to empty string
   *
   * @param id
   */
  function handleSetIsClicked(id: string) {
    setIsClicked(isClicked == id ? "" : id);
  }

  return (
    <div className={`${feraVisibility} w-2/3 p-3`}>
      <h1 className={titleClass}>Grille Fera</h1>
      <div className={"w-full flex-col md:flex-row flex"}>
        {/* External facts */}
        <GetFacts
          scene={props.data.scene}
          factName={"l_External_facts"}
          facts={props.data.faitsExterieurs}
          t={t}
          isClicked={isClicked}
          setIsClicked={handleSetIsClicked}
          annotationButtonClass={annotationButtonClass}
          onClick={() =>
            props.annotationCallBack(`faitsExterieurs`, t("l_ExternalFact"))
          }
        />

        {/* Internal facts */}
        <GetFacts
          scene={props.data.scene}
          facts={props.data.faitsInterieurs}
          factName={"l_Internal_facts"}
          t={t}
          annotationButtonClass={annotationButtonClass}
          isClicked={isClicked}
          setIsClicked={handleSetIsClicked}
          onClick={() =>
            props.annotationCallBack(`faitsInterieurs`, t("l_InternalFact"))
          }
        />
      </div>

      {/* Phrase / Emotions / Reasons */}
      <div className="flex items-center">
        <h2 className={subTitleClass}>{t("l_phrase_emotion_reason_title")}</h2>
        <h2 className={seuilClass}>(Seuil : {props.data.threshold})</h2>
        <div className="flex justify-center">
          <div className="mb-3 ml-5 xl:w-50">
            <select
              defaultValue="5"
              id="select_emotion"
              className="form-select appearance-none
      block
      w-1/2
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-1
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              onChange={selectEmotion}
            >
              <option value="5">{t("l_emotion_all")}</option>
              <option value="0">{t("l_emotion-0")}</option>
              <option value="1">{t("l_emotion-1")}</option>
              <option value="2">{t("l_emotion-2")}</option>
              <option value="3">{t("l_emotion-3")}</option>
              <option value="4">{t("l_emotion-4")}</option>
            </select>
          </div>
        </div>
      </div>
      <ul className={"list-decimal text-sm w-full"}>
        {props.data.phrases
          .map((p, phraseIndex) => {
            return { phrase: p, phraseIndex };
          })
          .filter((p: { phrase: Phrase; phraseIndex: number }) => {
            if (emotionSelected == 5) {
              return true;
            }
            return p.phrase.emotion == emotionSelected;
          })
          .map((p: { phrase: Phrase; phraseIndex: number }) => {
            return (
              <li
                key={`phrase-list-${p.phraseIndex}`}
                className="phrase-list w-full"
              >
                {/* Phrase */}
                <h3 className={phraseClass}>
                  {props.data.scene.slice(p.phrase.debut, p.phrase.fin)}
                </h3>

                {/* Flexbox for emotion & raison */}
                <div className="flex flex-row sm:flex-col w-4/5 my-3 border-b border-gray-200 justify-between">
                  {/* Emotion */}
                  <div className="w-1/3 mr-4 mb-2">
                    <h4 className={emotionRaisonClass}>{`${t(
                      "l_Emotion"
                    )} `}</h4>
                    <DropdownButton
                      key={`emotion-selector-${p.phraseIndex}`}
                      className={`${hide} w-full`}
                      id="dropdown-basic-button"
                      title={t("l_emotion-" + p.phrase.emotion)}
                    >
                      {/* Emotions choices */}
                      {Object.keys(Emotions)
                        .filter((key: string) => !isNaN(Number(key)))
                        .map((key: string) => {
                          return (
                            <Dropdown.Item
                              className={"w-full caca"}
                              key={`phrase-${p.phraseIndex}-emotion-selection-${key}`}
                              onClick={() =>
                                props.emotionCallback(
                                  `phrases[${p.phraseIndex}].emotion`,
                                  Number(key)
                                )
                              }
                            >
                              {t("l_emotion-" + key)}
                            </Dropdown.Item>
                          );
                        })}
                    </DropdownButton>
                  </div>

                  {/* Reasons */}
                  <div className="w-1/2 flex flex-col self-end">
                    {/* Reasons choice */}
                    <div className="mr-4 mb-2">&nbsp;</div>
                    <AnnotationButton
                      onClick={() =>
                        props.annotationCallBack(
                          `phrases[${p.phraseIndex}].raison`,
                          t("l_ReasonNumber") + " " + (p.phraseIndex + 1)
                        )
                      }
                      isClicked={isClicked}
                      handleSetIsClicked={handleSetIsClicked}
                      keyName={`reason-button-${p.phraseIndex}`}
                      className={`${annotationButtonClass} mb-2 w-1/2`}
                      t={t}
                      i18nBtnName={"l_Reasons"}
                    />
                    <div className="h-1/2">
                      {p.phrase.raison.map(
                        (raison: Annotable, reasonIndex: number) => (
                          <p
                            key={`reason-${reasonIndex}`}
                            className={"reason-text text-justify"}
                          >
                            {props.data.scene.slice(raison.debut, raison.fin)}
                          </p>
                        )
                      )}
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
      </ul>

      {/* Participants */}
      <h2 className={subTitleClass}>{t("l_Participants")}</h2>
      <ul className={"flex w-full flex-col flex-wrap pl-0 pt-4"}>
        {/* Each participant */}
        {props.data.participants.map(
          (participant: Participant, index: number) => {
            return (
              <li key={index} className="pb-4">
                <div className={"flex flex-row align-items-start"}>
                  <button
                    className={
                      "py-3 bg-default text-gray-700 font-semibold hover:bg-red-300 px-4 border border-red-500 hover:border-transparent rounded"
                    }
                    onClick={() => props.removeParticipant(index)}
                    key={`participant-removal-btn-${index}`}
                  >
                    X
                  </button>
                  <FloatingLabel
                    onChange={(event: any) =>
                      props.roleCallback(
                        `participants[${index}].role`,
                        event.target.value
                      )
                    }
                    className={`${hide} mx-10`}
                    controlId="floatingInput"
                    label={t("l_role")}
                  >
                    <Form.Control
                      type="textarea"
                      value={
                        participant.role ??
                        t("l_ParticipantNumber") + (index + 1)
                      }
                    />
                  </FloatingLabel>
                  <div className="ml-3">
                    <AnnotationButton
                      onClick={() =>
                        props.annotationCallBack(
                          `participants[${index}].qualificateur`,
                          t("l_ParticipantNumber") + (index + 1)
                        )
                      }
                      isClicked={isClicked}
                      handleSetIsClicked={handleSetIsClicked}
                      keyName={`qualifiers-${index}`}
                      className={qualifiersButtonClass}
                      t={t}
                      i18nBtnName={"l_Qualifiers"}
                    />

                    {participant.qualificateur.map((q: Qualificateur) => (
                      <p>{props.data.scene.slice(q.debut, q.fin)}</p>
                    ))}
                  </div>
                </div>
              </li>
            );
          }
        )}
      </ul>
      <button
        className={annotationButtonClass}
        onClick={() => props.addParticipantCallBack()}
      >
        {t("l_add_participant")}
      </button>
      <hr />
      {/* Actions */}
      <div className={"w-full flex-col md:flex-row flex"}>
        <div className={"w-full"}>
          {/* Actions Exterieurs*/}
          <h2 className={subTitleClass}>{t("l_External_actions")}</h2>
          <ul className={"flex w-full flex-col flex-wrap"}>
            {/* Each action */}
            {props.data.actionsExterieurs.map(
              (action: Action, index: number) => {
                return (
                  <li key={index} className="md:w-1/2 w-full h-1/2">
                    <div
                      className={"flex flex-row items-center justify-between"}
                    >
                      <div className={"w-full"}>
                        <textarea
                          onChange={(event: ChangeEvent<HTMLTextAreaElement>) =>
                            props.roleCallback(
                              `actionsExterieurs[${index}].text`,
                              event.target.value
                            )
                          }
                          className={classNameActionsTextArea}
                          value={action.text}
                        />
                      </div>
                      <button
                        className={
                          "m-3 bg-default text-gray-700 font-semibold hover:bg-red-300 py-1 px-2 border border-red-500 hover:border-transparent rounded"
                        }
                        onClick={() =>
                          props.removeActionExterieurCallBack(index)
                        }
                      >
                        X
                      </button>
                    </div>
                  </li>
                );
              }
            )}
          </ul>
          <button
            className={`${annotationButtonClass} items-center mb-2`}
            onClick={() => props.addActionCallBack("actionsExterieurs")}
            key={`add-external-action`}
          >
            {t("l_add")}
          </button>
        </div>
        <div className={"w-full"}>
          {/* Actions Interieurs */}
          <h2 className={subTitleClass}>{t("l_Internal_actions")}</h2>
          <ul className={"flex w-full flex-col flex-wrap"}>
            {/* Each action */}
            {props.data.actionsInterieurs.map(
              (action: Action, index: number) => {
                return (
                  <li key={index} className="md:w-1/2 w-full h-1/2">
                    <div
                      className={"flex flex-row items-center justify-between"}
                    >
                      <div className={"w-full"}>
                        <textarea
                          onChange={(event: ChangeEvent<HTMLTextAreaElement>) =>
                            props.roleCallback(
                              `actionsInterieurs[${index}].text`,
                              event.target.value
                            )
                          }
                          className={classNameActionsTextArea}
                          value={action.text}
                        />
                      </div>
                      <button
                        className={
                          "m-3 bg-default text-gray-700 font-semibold hover:bg-red-300 py-1 px-2 border border-red-500 hover:border-transparent rounded"
                        }
                        onClick={() =>
                          props.removeActionInterieurCallBack(index)
                        }
                        key={`remove-internal-action-btn-${index}`}
                      >
                        X
                      </button>
                    </div>
                  </li>
                );
              }
            )}
          </ul>
          <button
            className={annotationButtonClass}
            onClick={() => props.addActionCallBack("actionsInterieurs")}
          >
            {t("l_add_participant")}
          </button>
        </div>
      </div>
      <hr />
      <button
        className={annotationButtonClass}
        onClick={props.saveAnnotationsCallback}
      >
        {t("l_save_changes")}
      </button>
    </div>
  );
}

export default withTranslation()(Fera);
