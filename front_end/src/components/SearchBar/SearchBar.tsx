import React, { useState } from "react";
import { withTranslation, WithTranslation } from "react-i18next";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import axios from "axios";
import config from "../../config";
import Notification from "../Notifications/Notification";
import SearchModal from "../Modal/SearchModal";
import { Option } from "react-bootstrap-typeahead/types/types";

type Item = {
  id: number;
  titre: string;
  sceneSnippet: string;
};

interface SearchBarProps extends WithTranslation {
  fetchScene: (id: string) => boolean;
}

/**
 * This function aim to implement t
 * @param props
 * @constructor
 */
function SearchBar(props: SearchBarProps) {
  const [selectedItem, setSelectedItem] = useState<Item>({
    id: -1,
    titre: "",
    sceneSnippet: "",
  });
  const [options, setOptions] = useState<Item[]>([]);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { t, fetchScene } = props;

  /**
   * While searching, we get from the server the string typed.
   * Then, set the options with the received data.
   * @param string The string typed
   */
  const handleOnSearch = (string: string) => {
    setIsLoading(true);
    if (!string) {
      setOptions([]);
      return;
    }
    axios
      .get(config.apiUrl + config.apiRoutes.search + "/" + string)
      .then((response) => {
        setOptions(response.data);
      })
      .catch(function (error) {
        console.log(error);
        Notification({
          duration: 5000,
          message: error?.response?.data?.message ?? t("l_search_error_msg"),
          title: t("l_error"),
          type: "danger",
        });
      })
      .finally(() => setIsLoading(false));
  };

  /**
   * When select an item, display a modal to ask the participant
   * @param option List of item
   */
  const handleOnSelect = (option: Option) => {
    const item = option as Item[];
    if (item.length == 0) {
      return;
    }
    setSelectedItem(item[0]);
    setShowModal(true);
  };

  /**
   * This is how the result is displayed into the autocompletion search bar
   * @param option
   */
  const formatResult = (option: Option) => {
    const item = option as Item;
    return (
      <div className="flex flex-col mb-3">
        <span className="flex-wrap font-bold">{item.titre}</span>
        <span className="flex-wrap">{item.sceneSnippet}</span>
      </div>
    );
  };

  return (
    <div className="position-relative z-50">
      <div className="flex flex-row items-center">
        <AsyncTypeahead
          id="async-search"
          className={"w-full px-4 mx-4"}
          delay={100}
          isLoading={false}
          onSearch={handleOnSearch}
          options={options}
          filterBy={() => true}
          labelKey="titre"
          emptyLabel={t("l_no_result")}
          placeholder={t("l_search_placeholder")}
          renderMenuItemChildren={(option) => formatResult(option)}
          onChange={handleOnSelect}
        />
        {isLoading ? (
          <div
            className="mx-4 rbt-loader spinner-border spinner-border-sm"
            role="status"
          >
            <span className="sr-only visually-hidden">Loading...</span>
          </div>
        ) : null}
      </div>

      <SearchModal
        showModal={showModal}
        setShowModal={setShowModal}
        fetchScene={fetchScene}
        id={selectedItem?.id}
        title={selectedItem?.titre}
        text={selectedItem?.sceneSnippet}
      />
    </div>
  );
}

export default withTranslation()(SearchBar);
