import { withTranslation, WithTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import "../../../assets/styles/Form.css";
import ProgressBar from "@ramonak/react-progress-bar";
import { FloatingLabel, Form } from "react-bootstrap";
import Notification from "../../Notifications/Notification";
import HandlerImportScene from "../../DataHandler/HandlerNewScene/HandlerImportScene";
import { fetchProgress, launchAnalysis } from "../../../utils";
import { AnalysisResponse } from "../../../utils/interfaces";

interface IProps extends WithTranslation {
  updateSceneCallBack: (scene: string | ArrayBuffer | null) => void;
  updateSceneTitle: (title: string) => void;
  updateThreshold: (threshold: number) => void;
  setId: (id: string) => void;
  scene: string;
  title: string;
  threshold: number;
}

interface ProgressProps {
  segmentation: number;
  faits: number;
  participants: number;
  emotions: number;
  raisons: number;
  error: string;
}

function NewScene(props: IProps) {
  const { t, title, scene } = props;
  const [idScene, setIdScene] = useState("");
  const [filled, setFilled] = useState({ title: false, scene: false });
  const [progress, setProgress] = useState({
    segmentation: 0,
    faits: 0,
    participants: 0,
    emotions: 0,
    raisons: 0,
    error: "",
  });

  useEffect(() => {
    props.updateSceneTitle("");
    props.updateSceneCallBack("");
    props.updateThreshold(0.3);
  }, []);

  function handleProgress(id: string) {
    const intervalID = setInterval(() => {
      fetchProgress(id).then((res) => {
        if (res.error !== null) {
          clearInterval(intervalID);
          return;
        }
        setProgress({ ...progress, ...res });
        if (getProgressInPercentage({ ...progress, ...res }) >= 100) {
          Notification({
            title: t("l_success_analysis_title"),
            message: t("l_success_analysis_msg"),
            type: "success",
            duration: 3000,
          });
          clearInterval(intervalID);
          props.setId(id);
        }
      });
    }, 4000);
  }

  function getProgressInPercentage(props: ProgressProps) {
    let progress = 0;
    const { raisons, faits, segmentation, participants, emotions } = props;
    const currentProgress = {
      raisons,
      faits,
      segmentation,
      participants,
      emotions,
    };
    Object.values(currentProgress).forEach((p) => {
      if (p !== 0) {
        progress++;
      }
    });
    return progress * (100 / Object.keys(currentProgress).length);
  }

  /**
   * If the analysis is in progress, return false a
   */
  function analysisInProgress() {
    if (idScene.length > 0) {
      if (getProgressInPercentage(progress) < 100) {
        Notification({
          duration: 5000,
          message: t("l_analysis_running_msg"),
          title: t("l_error"),
          type: "danger",
        });
        return true;
      }
      setIdScene("");
      setProgress({
        segmentation: 0,
        faits: 0,
        participants: 0,
        emotions: 0,
        raisons: 0,
        error: "",
      });
    }
    return false;
  }

  function handleSubmit(event: { preventDefault: () => void }) {
    if (analysisInProgress()) {
      event.preventDefault();
      return;
    }
    if (title === "" || scene === "") {
      Notification({
        duration: 5000,
        message: t("l_missing_title_or_scene_text"),
        title: t("l_missing_info"),
        type: "danger",
      });
      event.preventDefault();
      return;
    }
    launchAnalysis(title, scene, props.threshold).then(
      (res: AnalysisResponse) => {
        if (res.error !== null) {
          Notification({
            duration: 5000,
            message: res.error.toString() ?? t("l_analysis_error"),
            title: t("l_error"),
            type: "danger",
          });
          return;
        }
        const id = res.id.toString();
        setIdScene(id);
        handleProgress(id);
      }
    );
    event.preventDefault();
  }

  /**
   * Test if the required field are filled
   */
  function requiredFieldOk() {
    return Object.values(filled).every((value) => value);
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className={"flex flex-row "}>
          <HandlerImportScene
            setFilled={setFilled}
            updateSceneCallback={props.updateSceneCallBack}
            updateSceneTitle={props.updateSceneTitle}
          />
          <button
            className={`${
              requiredFieldOk()
                ? "bg-analysis text-white border"
                : "disabled text-black border-analysis border-2 bg-white transition-all ease-in-out duration-500"
            }  hover:bg-pink-500 py-4 px-10 mx-4 rounded-lg `}
            type="submit"
          >
            {t("l_analysis_button")}
          </button>

          <FloatingLabel
            onBlur={(event: any) => {
              const threshold = Number(event.target.value);
              if (threshold < 0.01 || threshold > 0.99) {
                Notification({
                  title: t("l_title_threshold"),
                  type: "warning",
                  message: t("l_msg_threshold"),
                  duration: 3000,
                });
                event.target.value = threshold > 0.99 ? 0.95 : 0.3;
              }
              props.updateThreshold(event.target.value);
            }}
            onChange={(event: any) => props.updateThreshold(event.target.value)}
            controlId="floatingInputThreshold"
            label={t("l_threshold")}
          >
            <Form.Control
              type="number"
              step={0.05}
              min={0.05}
              max={0.95}
              defaultValue={String(0.3)}
              className="min-h-full"
            />
          </FloatingLabel>
        </div>

        <FloatingLabel
          className={"mt-3"}
          onChange={(event: any) => {
            setFilled({ ...filled, title: !!event.target.value });
            props.updateSceneTitle(event.target.value);
          }}
          controlId="floatingInput"
          label={t("l_scene_title")}
        >
          <Form.Control
            value={title}
            type="textarea"
            className={`${filled.title ? "" : "border-2 border-danger"}`}
            placeholder={t("l_insert_title_here")}
          />
        </FloatingLabel>
        <textarea
          value={props.scene}
          onChange={(event: any) => {
            setFilled({ ...filled, scene: !!event.target.value });
            props.updateSceneCallBack(event.target.value);
          }}
          className={`${
            filled.scene ? "" : "border-2 border-danger focus:ring-danger"
          } w-full focus:shadow-default focus:shadow-md border rounded-lg h-96 my-3 focus:ring-4 transition-all ease-in-out`}
        />
        <br />
      </form>
      <div className={idScene ? "" : "hide"}>
        <ProgressBar completed={getProgressInPercentage(progress)} />
        <div className={"m-2 flex justify-between"}>
          {Object.entries(progress)
            .filter(([key]: [string, string | number]) => key != "error")
            .map(([key, value]: [string, string | number]) => {
              return (
                <ProgressCircle
                  key={key}
                  progressValue={Number(value)}
                  name={key}
                />
              );
            })}
        </div>
      </div>
    </>
  );
}
/**
 * Construct a circle which indicates the success/fail of a process
 * @param props progressValue - The progress value refers to :
 *              -1 : Error
 *              0 : Not visited yet (keep the current color)
 *              1 : Success
 *              name - Name to put beside the circle
 * @constructor
 */
function ProgressCircle(props: { progressValue: number; name: string }) {
  // By default, put the circle in gray
  const { progressValue, name } = props;
  const [circle, setCircle] = useState(
    <Circle isLoading={true} color={"gray"} name={name} />
  );

  // Modify circle when progress is changing
  useEffect(() => {
    modifyCircle();
  }, [progressValue]);

  /**
   * Modify Circle color according to the state provided
   */
  function modifyCircle() {
    if (progressValue != 0) {
      setCircle(
        progressValue == -1 ? (
          <Circle color={"red"} isLoading={false} name={name} />
        ) : (
          <Circle color={"green"} isLoading={false} name={name} />
        )
      );
    }
  }

  return circle;
}

/**
 * Represent a circle in a tailwind way
 * @param props color
 * @constructor
 */
function Circle(props: {
  color: "gray" | "red" | "green" | "orange";
  name: string;
  isLoading: boolean;
}) {
  const { color, name, isLoading } = props;
  return (
    <div className={"flex fill-interface-button flex-col text-center w-full"}>
      {isLoading ? (
        <div
          className="mx-4 rbt-loader spinner-border spinner-border-sm mx-auto"
          role="status"
        >
          <span className="sr-only visually-hidden">Loading...</span>
        </div>
      ) : (
        <div className={`bg-${color}-500 w-4 h-4 rounded-full m-auto`} />
      )}
      <p className={"capitalize mt-2"}>{name}</p>
    </div>
  );
}

export default withTranslation()(NewScene);
