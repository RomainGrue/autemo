import { withTranslation } from "react-i18next";
import React, { Component } from "react";
import "../../../assets/styles/Fera.css";
import { TextAnnotator } from "react-text-annotate";
import { Span } from "react-text-annotate/lib/span";
import { Annotation, TAG_COLOR } from "../../../utils/interfaces";

interface IProps {
  updateCallBack: (a: Annotation[], tag: string, length: number) => void;
  scene: string;
  annotation: {
    value: Span[];
    tag: string;
    path: string;
  };
}

class EditScene extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  componentDidMount() {
    return true;
  }

  render() {
    // const { t } = this.props;
    return (
      <TextAnnotator
        content={this.props.scene}
        value={this.props.annotation.value}
        onChange={(value: any) => {
          this.props.updateCallBack(
            value,
            this.props.annotation.tag,
            this.props.scene.length
          );
        }}
        getSpan={(span: any) => ({
          ...span,
          tag: this.props.annotation.tag,
          color: TAG_COLOR.DEFAULT,
        })}
      />
    );
  }
}

export default withTranslation()(EditScene);
