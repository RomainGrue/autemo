import React from "react";
import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import EditScene from "./EditScene";
import { TextAnnotator } from "react-text-annotate";

Enzyme.configure({ adapter: new Adapter() });

describe("Reasons#EditScene component", () => {
  const annotate = jest.fn(
    (value: { [key: string]: any }[], tag: string) => {}
  );

  const wrapper = Enzyme.mount(
    <EditScene
      updateCallBack={annotate}
      scene={"lorem ipsum"}
      annotation={{ value: [], tag: "tag", path: "" }}
    />
  );

  it("Reasons#Should find annotator", () => {
    expect(wrapper.find("TextAnnotator").exists()).toBe(true);
    wrapper.findWhere((node) => node.key() === "main-fera");
  });
});
