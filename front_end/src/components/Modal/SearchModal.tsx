import React, { useEffect, useState } from "react";
import { WithTranslation, withTranslation } from "react-i18next";
import axios from "axios";
import config from "../../config";
import Notification from "../Notifications/Notification";

interface ModalProps extends WithTranslation {
  showModal: boolean;
  setShowModal: (show: boolean) => void;
  title: string | undefined;
  text: string | undefined;
  fetchScene: (id: string) => boolean;
  id: number;
}

/**
 * Modal used to display a FERA element to the user, with the title and the scene
 * A callback must be provided when the user click on the import button.
 *
 * @param props ModalProps
 * @constructor
 */
function SearchModal(props: ModalProps) {
  const { showModal, setShowModal, title, text, fetchScene, id, t } = props;
  const [fetchText, setFetchText] = useState(text);
  const [isLoading, setIsLoading] = useState(true);

  const fillText = () => {
    const url = config.apiUrl + config.apiRoutes.analysisRoute + id;
    console.log(url);
    setIsLoading(true);
    axios
      .get(url)
      .then((response) => {
        console.log(response);
        setFetchText(response.data.text);
      })
      .catch((error) => {
        console.log(error);
        Notification({
          duration: 5000,
          message: error?.response?.data?.message ?? t("l_error_fetch_scene"),
          title: `Error ${error.code}`,
          type: "danger",
        });
      })
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    if (showModal) {
      fillText();
    }
  }, [showModal]);

  return (
    <>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-7xl">
              {/*content*/}
              <div className="border-0 rounded shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-xl font-semibold">
                    {title ?? "Titre inconnu"}
                  </h3>
                  <button
                    className="p-1 ml-auto border-0 text-black text-3xl font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="text-gray-900 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <p className="my-4 text-blueGray-500 text-lg leading-relaxed">
                    {isLoading ? text : fetchText}
                  </p>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b">
                  <button
                    className="font-bold text-red-500 uppercase px-6 py-3 text-sm rounded border-1 hover:shadow hover:bg-red-600 hover:text-white outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Annuler
                  </button>
                  <button
                    className="font-bold bg-blue-500 hover:bg-blue-700 uppercase text-white text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => {
                      fetchScene(id.toString()) && setShowModal(false);
                    }}
                  >
                    Importer
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black" />
        </>
      ) : null}
    </>
  );
}

export default withTranslation()(SearchModal);
