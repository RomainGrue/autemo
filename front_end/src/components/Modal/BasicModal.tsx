import React from "react";
import { WithTranslation, withTranslation } from "react-i18next";

interface ModalProps extends WithTranslation {
  showModal: boolean;
  setShowModal: (show: boolean) => void;
  setConfirm: (confirm: boolean) => void;
  body: string;
  title: string;
}

/**
 * Modal used to display a message with a button to accept or cancel.
 * You must provide a callback to get the value of the button
 *
 * @param props ModalProps
 * @constructor
 */
function BasicModal(props: ModalProps) {
  const { showModal, setShowModal, setConfirm, body, title, t } = props;

  return (
    <>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-7xl">
              {/*content*/}
              <div className="border-0 rounded shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-xl font-semibold">{title ?? ""}</h3>
                  <button
                    className="p-1 ml-auto border-0 text-black text-3xl font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="text-gray-900 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <p className="my-4 text-blueGray-500 text-lg leading-relaxed">
                    {body}
                  </p>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b">
                  <button
                    className="font-bold text-red-500 uppercase px-6 py-3 text-sm rounded border-1 hover:shadow hover:bg-red-600 hover:text-white outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    {t("l_cancel")}
                  </button>
                  <button
                    className="font-bold bg-blue-500 hover:bg-blue-700 uppercase text-white text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setConfirm(true)}
                  >
                    {t("l_confirm")}
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black" />
        </>
      ) : null}
    </>
  );
}

export default withTranslation()(BasicModal);
