import { Store } from "react-notifications-component";

export default function Notification(props: {
  title: string;
  message: string;
  type: "success" | "danger" | "info" | "default" | "warning";
  duration: number;
}) {
  const { title, message, type, duration } = props;

  return Store.addNotification({
    title: title ?? "Title",
    message: message ?? "Message",
    type: type ?? "info",
    insert: "top",
    container: "top-right",
    animationIn: ["animate__animated", "animate__fadeIn"],
    animationOut: ["animate__animated", "animate__fadeOut"],
    dismiss: {
      duration: duration ?? 5000,
      onScreen: true,
    },
  });
}
