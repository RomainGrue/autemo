import { withTranslation, WithTranslation } from "react-i18next";
import React, { Component } from "react";
import { Action, Annotable, FERA, Phrase } from "../../../utils/interfaces";
import { saveAs } from "file-saver";
import {
  BorderStyle,
  Document,
  HeadingLevel,
  Packer,
  Paragraph,
  Table,
  TableCell,
  TableRow,
  WidthType,
  TextRun,
  TableOfContents,
} from "docx";

interface IProps extends WithTranslation {
  data: FERA;
}

class HandlerExportScene extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.generateDocx = this.generateDocx.bind(this);
    this.generateEmotions = this.generateEmotions.bind(this);
  }

  borderStyle = {
    top: {
      style: BorderStyle.DASH_DOT_STROKED,
      size: 4,
      color: "000000",
    },
    bottom: {
      style: BorderStyle.DASH_DOT_STROKED,
      size: 4,
      color: "000000",
    },
    left: {
      style: BorderStyle.DASH_DOT_STROKED,
      size: 2,
      color: "000000",
    },
    right: {
      style: BorderStyle.DASH_DOT_STROKED,
      size: 2,
      color: "000000",
    },
  };

  componentDidMount() {
    return true;
  }

  generateArrayOnEmotion(emotion: Phrase) {
    return new TableRow({
      children: [
        new TableCell({
          children: [
            new Paragraph({
              text: this.props.data.scene.slice(emotion.debut, emotion.fin),
            }),
          ],
          borders: this.borderStyle,
        }),
        new TableCell({
          children: emotion.raison.map((r) => {
            return new Paragraph({
              bullet: {
                level: 0,
              },
              text: this.props.data.scene.slice(r.debut, r.fin),
            });
          }),
          borders: this.borderStyle,
        }),
      ],
    });
  }

  generateParagraphForEmotion(emotionStr: string, array: TableRow[]) {
    if (array.length != 0) {
      array.unshift(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [
                    new TextRun({
                      text: "Phrases",
                      bold: true,
                    }),
                  ],
                }),
              ],
              borders: this.borderStyle,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [
                    new TextRun({
                      text: "Raisons",
                      bold: true,
                    }),
                  ],
                }),
              ],
              borders: this.borderStyle,
            }),
          ],
        })
      );
      return [
        new Paragraph({
          text: emotionStr,
          heading: HeadingLevel.HEADING_2,
          spacing: {
            after: 250,
            before: 350,
          },
        }),
        new Table({
          columnWidths: [4505, 4505],
          rows: array,
        }),
      ];
    } else {
      return [];
    }
  }

  generateEmotions() {
    const neutre: TableRow[] = [];
    const colere: TableRow[] = [];
    const peur: TableRow[] = [];
    const tristesse: TableRow[] = [];
    const joie: TableRow[] = [];

    this.props.data.phrases.forEach((emotion) => {
      switch (emotion.emotion) {
        case 1: {
          colere.push(this.generateArrayOnEmotion(emotion));
          break;
        }
        case 2: {
          peur.push(this.generateArrayOnEmotion(emotion));
          break;
        }
        case 3: {
          tristesse.push(this.generateArrayOnEmotion(emotion));
          break;
        }
        case 4: {
          joie.push(this.generateArrayOnEmotion(emotion));
          break;
        }
        default: {
          neutre.push(this.generateArrayOnEmotion(emotion));
          break;
        }
      }
    });
    return [
      new Paragraph({
        text: "Emotions",
        heading: HeadingLevel.HEADING_1,
        spacing: {
          before: 250,
        },
      }),
      ...this.generateParagraphForEmotion("Neutre", neutre),
      ...this.generateParagraphForEmotion("Colère", colere),
      ...this.generateParagraphForEmotion("Peur", peur),
      ...this.generateParagraphForEmotion("Tristesse", tristesse),
      ...this.generateParagraphForEmotion("Joie", joie),
    ];
  }

  generateTitle() {
    return [
      new Paragraph({
        text: "Grille FERA",
        heading: HeadingLevel.TITLE,
        spacing: {
          after: 250,
        },
      }),
    ];
  }

  generateDocx() {
    const { data } = this.props;
    const doc = new Document({
      sections: [
        {
          properties: {},
          children: [
            ...this.generateTitle(),
            ...this.generateScene(),
            ...this.generateFaits("Faits Extérieurs", data.faitsExterieurs),
            ...this.generateFaits("Faits Intérieurs", data.faitsInterieurs),
            ...this.generateEmotions(),
            ...this.generateActions(
              "Actions Extérieurs",
              data.actionsExterieurs
            ),
            ...this.generateActions(
              "Actions Intérieurs",
              data.actionsInterieurs
            ),
          ],
        },
      ],
    });
    Packer.toBlob(doc).then((blob) => {
      saveAs(blob, this.props.data.titre + ".docx");
      console.log("Document created successfully");
    });
  }

  generateScene() {
    const { data } = this.props;
    return [
      new Paragraph({
        text: "Scène",
        heading: HeadingLevel.HEADING_1,
        spacing: {
          after: 250,
          before: 250,
        },
      }),
      new Paragraph({
        text: data.scene,
        spacing: {
          after: 250,
        },
      }),
    ];
  }

  generateFaits(titre: string, faits: Annotable[]) {
    const { data } = this.props;
    const faitsArr = [];
    faitsArr.push(
      new Paragraph({
        text: titre,
        heading: HeadingLevel.HEADING_1,
        spacing: {
          after: 250,
          before: 250,
        },
      })
    );
    faits.forEach((fait: Annotable) => {
      faitsArr.push(
        new Paragraph({
          bullet: {
            level: 0,
          },
          text: data.scene.slice(fait.debut, fait.fin),
        })
      );
    });
    return faitsArr;
  }

  generateActions(titre: string, actionsArray: Action[]) {
    const actions = [];
    actions.push(
      new Paragraph({
        text: titre,
        heading: HeadingLevel.HEADING_1,
        spacing: {
          after: 250,
          before: 550,
        },
      })
    );
    actionsArray.forEach((actionE: Action) => {
      actions.push(
        new Paragraph({
          text: actionE.text,
        })
      );
    });
    return actions;
  }

  render() {
    // use for translation
    const { t } = this.props;
    // TODO : Create a default button with this class
    const annotationButtonClass = `flex flex-nowrap bg-default text-gray-700 font-semibold hover:bg-gray-300 py-2 px-4 border border-blue-500 hover:border-transparent rounded`;
    return (
      <div>
        <button className={annotationButtonClass} onClick={this.generateDocx}>
          {t("l_export")}
        </button>
      </div>
    );
  }
}

export default withTranslation()(HandlerExportScene);
