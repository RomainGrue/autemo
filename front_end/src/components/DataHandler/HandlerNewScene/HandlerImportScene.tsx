import { withTranslation, WithTranslation } from "react-i18next";
import React, { Dispatch, SetStateAction, useCallback, useEffect } from "react";
import { useDropzone } from "react-dropzone";
import Notification from "../../Notifications/Notification";

interface IProps extends WithTranslation {
  updateSceneCallback: (scene: string | ArrayBuffer | null) => void;
  updateSceneTitle: (title: string) => void;
  setFilled: Dispatch<SetStateAction<{ title: boolean; scene: boolean }>>;
}

/**
 * Handle the file input and fill the scene
 * @param props a callback to fill the scene
 * @constructor
 */
function HandlerImportScene(props: IProps) {
  const { updateSceneCallback, updateSceneTitle, setFilled, t } = props;

  /**
   * Happens when the user try to give a file
   */
  const onChange = useCallback((acceptedFiles) => {
    if (acceptedFiles.length <= 0) {
      return;
    }
    const file = acceptedFiles[0];
    updateSceneCallback(file);
    const reader = new FileReader();
    reader.onabort = () =>
      Notification({
        duration: 5000,
        message: t("l_cancel_file"),
        title: t("l_cancel"),
        type: "info",
      });
    reader.onerror = () =>
      Notification({
        duration: 5000,
        message: `${t("l_error_file_read_msg")} ${file.name}`,
        title: t("l_error_file_read_title"),
        type: "danger",
      });
    reader.onload = () => {
      const binaryStr = reader.result;
      const titleWithoutExtension = file?.name?.replace(".txt", "");
      updateSceneCallback(binaryStr);
      updateSceneTitle(titleWithoutExtension);
      setFilled({ scene: !!binaryStr, title: titleWithoutExtension !== "" });
      Notification({
        duration: 5000,
        message: `${t("l_the_file")} ${file.name} ${t("l_read_success")}`,
        title: t("l_success_reading_title"),
        type: "success",
      });
    };
    reader.readAsText(file);
  }, []);

  const { getRootProps, getInputProps, open, acceptedFiles, fileRejections } =
    useDropzone({
      accept: ".txt", // Files accepted
      maxFiles: 1, // Number maximum of files
      onDrop: onChange, // When user give a file
    });

  useEffect(() => {
    if (fileRejections.length > 0) {
      Notification({
        duration: 5000,
        message: t("l_file_extension_rejected"),
        title: t("l_error"),
        type: "danger",
      });
    }
  }, [fileRejections]);
  /**
   * Display all the files
   */
  const files = acceptedFiles.map((file) => (
    <li key={file.name}>{file.name}</li>
  ));

  return (
    <button
      type="button"
      className={
        "bg-blue-600 hover:bg-blue-700 hover:cursor-pointer px-4 rounded-lg text-white items-center"
      }
      onClick={open}
    >
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        {t("l_upload_file_msg")}
      </div>
      {files.length ? (
        <ul className={"text-sm p-0 mb-0"}>{files}</ul>
      ) : (
        <p className="p-0 mb-0">{t("l_no_file_selected")}</p>
      )}
    </button>
  );
}

export default withTranslation()(HandlerImportScene);
