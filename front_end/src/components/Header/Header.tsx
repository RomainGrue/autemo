import React, { useEffect, useState } from "react";
import { CorrespondingMapName, Mode } from "../../utils/interfaces";
import { enumKeys } from "../../utils";
import SearchBar from "../SearchBar/SearchBar";
import { withTranslation, WithTranslation } from "react-i18next";
import BasicModal from "../Modal/BasicModal";

interface HeaderProps extends WithTranslation {
  setMenu: (mode: Mode) => void;
  menuState: number;
  fetchScene: (id: string) => boolean;
}
/**
 * Header to put in each pages
 * @param props setMenu - To change the pressed menu button
 * @constructor
 */
function Header(props: HeaderProps) {
  const { setMenu, menuState, fetchScene, t } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [confirm, setConfirm] = useState(false);

  /**
   * Check if the given mode corresponding button must be disabled
   * @param mode Mode
   */
  const mustBeDisable = (mode: Mode) => {
    return mode === Mode.EDIT && Mode.NEW_ANALYSIS == menuState;
  };

  /**
   * When the user click on new analysis button, show a modal
   * Other way, change the menu
   * @param modeElement
   */
  const handleOnClick = (modeElement: Mode.EDIT | Mode.NEW_ANALYSIS) => {
    if (modeElement === Mode.NEW_ANALYSIS && menuState === Mode.EDIT) {
      setShowModal(true);
      return;
    }
    setMenu(modeElement);
  };

  /**
   * Handle when user click on confirm to change to new analysis
   */
  useEffect(() => {
    if (confirm) {
      setMenu(Mode.NEW_ANALYSIS);
      setConfirm(false);
      setShowModal(false);
    }
  }, [confirm]);

  return (
    <>
      <nav className="bg-white border-gray-200 rounded dark:bg-gray-800 pl-4 pr-4 ml-4 mr-4">
        <div className="flex justify-between mx-auto px-2 sm:px-4 py-2.5 align-items-center">
          <div className="flex flex-row w-4/5">
            <h1 className="self-center text-lg font-semibold text-3xl whitespace-nowrap dark:text-white">
              AutEmo
            </h1>
            <div className="pl-10 w-3/5 mx-auto">
              <SearchBar fetchScene={fetchScene} />
            </div>
          </div>
          {/* Menu buttons */}
          <div
            className="hidden md:block justify-between items-center "
            id="mobile-menu-3"
          >
            <ul className="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
              {enumKeys(Mode).map((key) => (
                <li key={`mode-${key.toString()}`}>
                  <button
                    className={` ${Mode[key] == menuState ? "border-b" : ""} ${
                      mustBeDisable(Mode[key])
                        ? "text-gray-300"
                        : "text-gray-700 hover:border-b"
                    } transition duration-500  block py-2 pr-4 pl-3  md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 uppercase`}
                    id={`menu-btn-${key}`}
                    onClick={() => {
                      handleOnClick(Mode[key]);
                    }}
                    disabled={mustBeDisable(Mode[key])}
                  >
                    {CorrespondingMapName.get(Mode[key])}
                  </button>
                </li>
              ))}
            </ul>
          </div>

          {/* Burger button for small screen */}
          <button
            data-collapse-toggle="mobile-menu-3"
            type="button"
            className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
            aria-controls="mobile-menu-3"
            aria-expanded="false"
            onClick={() => setIsOpen(!isOpen)}
          >
            <svg
              className="w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              />
            </svg>
            <svg
              className="hidden w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        </div>

        {/* Small Menu Button */}
        {isOpen ? (
          <ul className="md:hidden pl-0 flex flex-col md:font-medium w-full">
            {enumKeys(Mode).map((key) => (
              <li>
                <button
                  className={` ${
                    mustBeDisable(Mode[key])
                      ? "text-gray-300"
                      : "text-gray-700 hover:bg-gray-50"
                  } w-full uppercase   md:hover:bg-transparent md:hover:text-blue-700 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700`}
                  id={`menu-btn-${key}`}
                  onClick={() => {
                    setMenu(Mode[key]);
                  }}
                  disabled={mustBeDisable(Mode[key])}
                >
                  {CorrespondingMapName.get(Mode[key])}
                </button>
              </li>
            ))}
          </ul>
        ) : null}
      </nav>

      <BasicModal
        body={t("l_confirm_body")}
        title={t("l_confirm_title")}
        showModal={showModal}
        setShowModal={setShowModal}
        setConfirm={setConfirm}
      />
    </>
  );
}

export default withTranslation()(Header);
