import axios from "axios";
import config from "../config";
import { AnalysisResponse, StateUpdatePayload } from "./interfaces";

/**
 * Function that returns string keys from enum
 * @param obj
 */
export function enumKeys<O extends object, K extends keyof O = keyof O>(
  obj: O
): K[] {
  return Object.keys(obj).filter((k) => Number.isNaN(+k)) as K[];
}

export async function launchAnalysis(
  titre: string,
  scene: string,
  threshold: number
): Promise<AnalysisResponse> {
  return axios
    .post(config.apiUrl + config.apiRoutes.analysisRoute, {
      titre,
      scene,
      threshold,
    })
    .then((response) => {
      console.log(response.data);
      const { id, titre, scene, error } = response.data;
      return { id, titre, scene, error };
    })
    .catch((error) => {
      console.log(error);
      return { id: -1, titre: "", scene: "", error };
    });
}

export async function fetchProgress(id: string): Promise<StateUpdatePayload> {
  return await axios
    .get(config.apiUrl + config.apiRoutes.analysisState + id) // http://localhost:8080/API/ + analysys-state/ + id
    .then((response) => {
      const { segmentation, emotions, participants, faits, raisons, error } =
        response.data;
      return { segmentation, emotions, participants, raisons, faits, error };
    })
    .catch(function (error) {
      return {
        segmentation: -1,
        emotions: -1,
        participants: -1,
        raisons: -1,
        faits: -1,
        error,
      };
    });
}
