export enum Mode {
  EDIT,
  NEW_ANALYSIS,
}

export const CorrespondingMapName = new Map([
  [Mode.EDIT, "Éditer"],
  [Mode.NEW_ANALYSIS, "Nouvelle analyse"],
]);

export enum Emotions {
  NEUTRAL = 0,
  ANGER = 1,
  FEAR = 2,
  SADNESS = 3,
  JOY = 4,
}

export interface AnalysisResponse {
  id: number;
  titre: string;
  scene: string;
  error: string;
}

export interface Action {
  id: number;
  text: string;
}

export interface Token {
  debut: number;
  fin: number;
  tag: string;
}

export interface Annotation {
  start: number;
  end: number;
}

export interface Annotable {
  id: number;
  debut: number;
  fin: number;
}

export interface Qualificateur {
  debut: number;
  fin: number;
}

export interface Participant {
  qualificateur: Qualificateur[];
  role: string | null;
}

export interface Phrase {
  debut: number;
  fin: number;
  emotion: number;
  raison: Annotable[];
}

export interface FERA {
  titre: string;
  scene: string;
  threshold: number;
  participants: Participant[];
  phrases: Phrase[];
  faitsInterieurs: Annotable[];
  faitsExterieurs: Annotable[];
  actionsInterieurs: Action[];
  actionsExterieurs: Action[];
  error: string;
}

export interface StateUpdatePayload {
  raisons: number;
  faits: number;
  segmentation: number;
  participants: number;
  emotions: number;
  error: string;
}

export enum TAG_COLOR {
  DEFAULT = "#ff5733",
  Emotion = "#2197C4FF",
}
