import React from "react";
import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { Home } from "./Home";
import { Mode } from "../../utils/interfaces";
import i18n from "../../i18n";
import _ from "lodash";
import { act } from "react-dom/test-utils";

Enzyme.configure({ adapter: new Adapter() });

describe("Reasons#Home", () => {
  const state = {
    id: "42",
    data: {
      scene:
        "Hier je suis allé manger avec Philippe. Je suis énervé et Alain aussi.",
      participants: [
        {
          qualificateur: [
            {
              debut: 58,
              fin: 63,
            },
          ],
          role: null,
        },
        {
          qualificateur: [
            {
              debut: 30,
              fin: 38,
            },
          ],
          role: null,
        },
      ],
      phrases: [
        {
          txt: "Hier je suis allé manger avec Philippe.",
          emotion: 0,
          raison: [],
        },
        {
          txt: "Je suis énervé et Alain aussi.",
          emotion: 1,
          raison: [
            {
              id: 10,
              debut: 6,
              fin: 25,
            },
          ],
        },
      ],
      faitsInterieurs: [
        {
          id: 10,
          debut: 0,
          fin: 20,
        },
        {
          id: 11,
          debut: 21,
          fin: 30,
        },
      ],
      faitsExterieurs: [
        {
          id: 13,
          debut: 5,
          fin: 12,
        },
      ],
      actionsInterieurs: [],
      actionsExterieurs: [],
      error: "null",
    },
    annotation: {
      value: [],
      tag: "",
      path: "",
    },
    mode: Mode.EDIT,
  };
  const wrapper = Enzyme.mount(
    <Home i18n={i18n} t={(text: string) => text} tReady />
  );
  it("Reasons#should render edit panel according to demand", () => {
    const testTag = "tag";
    const testPath = "phrases[1].raison";
    wrapper.setState(state);
    wrapper
      .findWhere((node) => node.key() === "main-fera")
      .prop("annotationCallBack")(testPath, testTag);
    wrapper.update();
    expect(_.get(wrapper.update().state(), "annotation.tag")).toBe(testTag);
    expect(_.get(wrapper.update().state(), "annotation.path")).toBe(testPath);
  });
});

/*describe("Change mode#Home", () => {
  const wrapper = Enzyme.mount(
    <Home i18n={i18n} t={(text: string) => text} tReady />
  );
  it("Toggle#should change state", () => {
    wrapper.setState({ mode: Mode.NEW_ANALYSIS });
    const btn = wrapper.find("#home-dropdown-basic-button");
    btn.at(2).simulate("click");
    wrapper.update();
    //console.log(wrapper.debug());
    const editBtn = wrapper.find("DropdownItem").at(0).simulate("click");
    expect(_.get(wrapper.update().state(), "mode")).toBe(Mode.EDIT);
  });
});*/
