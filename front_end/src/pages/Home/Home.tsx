import { withTranslation, WithTranslation } from "react-i18next";
import React, { Component, SyntheticEvent } from "react";
import "../../assets/styles/App.css";
import Header from "../../components/Header/Header";
import Fera from "../../components/Fera/Fera";
import axios from "axios";
import config from "../../config";
import Form from "../../components/ScenePanels/NewScene/NewScene";
import { Action, Annotable, FERA, Mode, Token } from "../../utils/interfaces";
import { Span } from "react-text-annotate/lib/span";
import EditScene from "../../components/ScenePanels/EditScene/EditScene";
import HandlerExportScene from "../../components/DataHandler/HandlerExportScene/HandlerExportScene";
import _ from "lodash";
import Notification from "../../components/Notifications/Notification";
import { isNaN } from "formik";

type IProps = WithTranslation;

interface IState {
  id: string;
  data: FERA;
  annotation: {
    value: Span[];
    tag: string;
    path: string;
  };
  mode: Mode;
}

export class Home extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      id: "",
      data: {
        titre: "",
        scene: "",
        threshold: 0.3,
        participants: [],
        phrases: [],
        faitsInterieurs: [],
        faitsExterieurs: [],
        actionsInterieurs: [],
        actionsExterieurs: [],
        error: "",
      },
      annotation: {
        value: [],
        tag: "",
        path: "",
      },
      mode: Mode.NEW_ANALYSIS,
    };
    this.setIdParent = this.setIdParent.bind(this);
    this.showAnnotation = this.showAnnotation.bind(this);
    this.updateAnnotation = this.updateAnnotation.bind(this);
    this.onEmotionChanged = this.onEmotionChanged.bind(this);
    this.onRoleChanged = this.onRoleChanged.bind(this);
    this.persistAnnotations = this.persistAnnotations.bind(this);
    this.addParticipant = this.addParticipant.bind(this);
    this.onMenuChange = this.onMenuChange.bind(this);
    this.removeParticipant = this.removeParticipant.bind(this);
    this.removeActionInterieur = this.removeActionInterieur.bind(this);
    this.removeActionExterieur = this.removeActionExterieur.bind(this);
    this.updateScene = this.updateScene.bind(this);
    this.updateTitle = this.updateTitle.bind(this);
    this.updateThreshold = this.updateThreshold.bind(this);
    this.addAction = this.addAction.bind(this);
  }

  componentDidMount() {
    return true;
  }

  updateAnnotation(value: Span[], tag: string) {
    if (this.state.annotation.path == "") {
      console.log("State annotation is null !");
      return;
    }
    value = this.correctSelectionNanBug(value);
    // Suppress if overlap
    Home.removeOverlappingSpan(value);

    const newVal = value.map((v) => {
      return {
        id: null,
        debut: v.start,
        fin: v.end,
      };
    });

    const dataTemp = this.state.data;
    _.set(dataTemp, this.state.annotation.path, newVal);
    this.setState({
      data: dataTemp,
      annotation: {
        tag,
        value: value,
        path: this.state.annotation.path,
      },
    });
  }

  /**
   * Correct the NaN when select all the text in TextAnnotator
   * @param value Span
   * @private
   */
  private correctSelectionNanBug(value: Span[]): Span[] {
    return value.filter((elt) => !isNaN(elt.start) && !isNaN(elt.end));
  }

  /**
   * Merge overlapping in span in value
   * @param value Span[]
   * @private
   */
  private static removeOverlappingSpan(value: Span[]) {
    value = value.sort((a, b) => a.start - b.start);
    for (let i = 1; i < value.length; i++) {
      if (value[i].start < value[i - 1].end) {
        let maxEnd =
          value[i].end > value[i - 1].end ? value[i].end : value[i - 1].end;
        let j = i + 1;
        while (j < value.length - 1 && maxEnd > value[j].start) {
          j++;
          if (value[j].end > maxEnd) {
            maxEnd = value[j].end;
          }
        }

        value[i - 1].end = maxEnd;
        value.splice(i, j - i + 1);
        break;
      }
    }
  }

  setIdParent(id: string): boolean {
    axios
      .get(config.apiUrl + config.apiRoutes.analysisRoute + id) // http://localhost:8080/API/ + analyse/ + id
      .then((response) => {
        console.log("Response Data: " + JSON.stringify(response.data));
        if (response.data.error != null) {
          const data = {
            ...this.state.data,
            ...{ error: response.data.error },
          };
          this.setState({ data, id, mode: Mode.EDIT });
          return;
        } else {
          const {
            id,
            participants,
            phrases,
            faits,
            actions,
            error,
            threshold,
            title,
            text,
          } = response.data;
          console.log("Response Data else: " + JSON.stringify(response.data));
          const faitsInterieurs: Annotable[] = [];
          const faitsExterieurs: Annotable[] = [];
          faits.map(
            (f: {
              id: number;
              interieur: boolean;
              debut: number;
              fin: number;
            }) => {
              const val = {
                id: f.id,
                debut: f.debut,
                fin: f.fin,
              };
              if (f.interieur) {
                faitsInterieurs.push(val);
              } else {
                faitsExterieurs.push(val);
              }
            }
          );
          const actionsInterieurs: Action[] = [];
          const actionsExterieurs: Action[] = [];
          actions.map((a: { id: number; interieur: boolean; text: string }) => {
            const val = {
              id: a.id,
              text: a.text,
            };
            if (a.interieur) {
              actionsInterieurs.push(val);
            } else {
              actionsExterieurs.push(val);
            }
          });
          this.setState({
            data: {
              titre: title,
              scene: text,
              threshold: threshold,
              participants,
              phrases,
              faitsInterieurs,
              faitsExterieurs,
              actionsInterieurs,
              actionsExterieurs,
              error,
            },
            id,
            annotation: this.state.annotation,
            mode: Mode.EDIT,
          });
          console.log(
            "Result Data: " +
              JSON.stringify(this.state.data.faitsInterieurs) +
              " ///////////////////" +
              JSON.stringify(this.state.data.faitsExterieurs)
          );
        }
        return true;
      })
      .catch(function (error) {
        console.log(error);
        Notification({
          duration: 5000,
          message: "Impossible de joindre le serveur",
          title: "Erreur d'importation",
          type: "danger",
        });
        return false;
      });
    //this.setState({ id }); useless ??
    return true;
  }

  showAnnotation(path: string, tag: string) {
    const data = this.state.data;
    const value = _.get(data, path);
    this.parseDataIntoTokens(path, tag, value);
  }

  parseDataIntoTokens(path: string, tag: string, tokens: Token[]) {
    const value = tokens.map((t) => {
      return {
        start: t.debut,
        end: t.fin,
        tag,
      };
    });
    this.setState({
      annotation: { tag, value, path },
    });
  }

  onMenuChange(mode: Mode) {
    this.setState({ mode });
    console.log(this.state);
  }

  onEmotionChanged(path: string, emotion: number) {
    const data = _.set(this.state.data, path, emotion);
    this.setState({ data });
  }

  onRoleChanged(path: string, role: string) {
    const data = _.set(this.state.data, path, role);
    this.setState({ data });
  }

  addAction(path: string) {
    const newA = [
      {
        id: null,
        text: "",
      },
    ];
    const newActions =
      path == "actionsInterieurs"
        ? [...this.state.data.actionsInterieurs, ...newA]
        : [...this.state.data.actionsExterieurs, ...newA];
    const data = _.set(this.state.data, path, newActions);
    this.setState({ data });
  }

  addParticipant() {
    const newP = [
      {
        qualificateur: [],
        role: null,
      },
    ];
    const newParticipants = [...this.state.data.participants, ...newP];
    const data = _.set(this.state.data, "participants", newParticipants);
    this.setState({ data });
  }

  removeActionInterieur(index: number) {
    if (index < 0 || this.state.data.actionsInterieurs.length < index) {
      throw new Error("Index of action out of bound");
    }
    this.state.data.actionsInterieurs.splice(index, 1);
    const data = _.set(
      this.state.data,
      "actions",
      this.state.data.actionsInterieurs
    );
    this.setState({ data });
  }

  removeActionExterieur(index: number) {
    if (index < 0 || this.state.data.actionsExterieurs.length < index) {
      throw new Error("Index of action out of bound");
    }
    this.state.data.actionsExterieurs.splice(index, 1);
    const data = _.set(
      this.state.data,
      "actions",
      this.state.data.actionsExterieurs
    );
    this.setState({ data });
  }

  /**
   * Remove a participant from the list of participants in the data
   * @param index index number of the participant in the list to remove
   */
  removeParticipant(index: number) {
    if (index < 0 || this.state.data.participants.length < index) {
      throw new Error("Index of participant out of bound");
    }
    this.state.data.participants.splice(index, 1);
    const data = _.set(
      this.state.data,
      "participants",
      this.state.data.participants
    );
    this.setState({ data });
  }

  /**
   * Refresh the scene from the text in the imported file
   * @param scene text from the file
   */
  updateScene(scene: string | ArrayBuffer | null) {
    if (scene === null) {
      return;
    }
    const data = _.set(this.state.data, "scene", scene.toString());
    this.setState({ data });
  }

  updateTitle(title: string) {
    const data = _.set(this.state.data, "titre", title.toString());
    this.setState({ data });
  }

  updateThreshold(threshold: number) {
    if (threshold >= 1 || threshold <= 0) {
      throw new Error("Threshold must be strictly positive and smaller than 1");
    }
    const data = _.set(this.state.data, "threshold", threshold);
    this.setState({ data });
  }

  persistAnnotations(e: SyntheticEvent) {
    const {
      participants,
      phrases,
      faitsInterieurs,
      faitsExterieurs,
      actionsInterieurs,
      actionsExterieurs,
      error,
    } = this.state.data;
    const fis = faitsInterieurs.map((fi) => {
      return { id: fi.id, debut: fi.debut, fin: fi.fin, interieur: true };
    });
    const fes = faitsExterieurs.map((fi) => {
      return { id: fi.id, debut: fi.debut, fin: fi.fin, interieur: false };
    });
    const aI = actionsInterieurs.map((ai) => {
      return { id: ai.id, text: ai.text, estinterieur: true };
    });
    const aE = actionsExterieurs.map((ae) => {
      return { id: ae.id, text: ae.text, estinterieur: false };
    });
    const faits = fis.concat(fes);
    const actions = aI.concat(aE);
    const newData = {
      id: this.state.id,
      participants,
      phrases,
      faits,
      actions,
      error,
    };
    axios
      .post(config.apiUrl + config.apiRoutes.persist, newData)
      .then((response) => {
        console.log(response);
        Notification({
          duration: 5000,
          message: response?.data?.message ?? "Analyse sauvegardé",
          title: "Succès",
          type: "success",
        });
      })
      .catch(function (error) {
        console.log(error);
        Notification({
          duration: 5000,
          message:
            error?.response?.data?.message ?? "Erreur lors de la sauvegarde",
          title: "Erreur",
          type: "danger",
        });
      });

    e.preventDefault();
  }

  render() {
    const mode = this.state.mode;
    let sceneDisplay;
    switch (+mode) {
      case Mode.EDIT:
        sceneDisplay = (
          <nav className="w-3/5 flex flex-col">
            <div className="fixed w-2/5 grid gap-4 grid-cols-1 h-4/5">
              <div className="w-full text-justify overflow-y-auto m-3 p-3 border rounded">
                <EditScene
                  updateCallBack={this.updateAnnotation}
                  scene={this.state.data.scene}
                  annotation={this.state.annotation}
                />
              </div>
              <div className="ml-3">
                <HandlerExportScene data={this.state.data} />
              </div>
            </div>
          </nav>
        );
        break;
      case Mode.NEW_ANALYSIS:
        sceneDisplay = (
          <div className="w-full p-3 m-4">
            <Form
              updateSceneCallBack={this.updateScene}
              updateSceneTitle={this.updateTitle}
              scene={this.state.data.scene}
              updateThreshold={this.updateThreshold}
              threshold={this.state.data.threshold}
              setId={this.setIdParent}
              title={this.state.data.titre}
            />
          </div>
        );
        break;
      default:
        sceneDisplay = <h1>Illegal mode</h1>;
        break;
    }
    return (
      <div>
        <Header
          setMenu={this.onMenuChange}
          menuState={this.state.mode}
          fetchScene={this.setIdParent}
        />
        <div className={"flex justify-items-stretch w-full"}>
          {sceneDisplay}
          <Fera
            key={"main-fera"}
            removeActionExterieurCallBack={this.removeActionExterieur}
            removeActionInterieurCallBack={this.removeActionInterieur}
            removeParticipant={this.removeParticipant}
            addActionCallBack={this.addAction}
            addParticipantCallBack={this.addParticipant}
            saveAnnotationsCallback={this.persistAnnotations}
            annotationCallBack={this.showAnnotation}
            emotionCallback={this.onEmotionChanged}
            roleCallback={this.onRoleChanged}
            id={this.state.id}
            data={this.state.data}
            mode={this.state.mode}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(Home);
