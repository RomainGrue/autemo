export default {
  apiUrl: "http://localhost:8080/API/",
  apiRoutes: {
    analysisRoute: "analyse/",
    analysisState: "analysis-state/",
    persist: "persist",
    search: "search",
  },
};
