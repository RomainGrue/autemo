# REACT FRONTEND BOILERPLATE 

## Description

Frontend 

## Overview



### Directory structure 

```bash
.
├── App.tsx
├── assets
│   ├── imgs
│   │   └── logo.svg
│   └── styles
│       ├── App.css
│       ├── index.css
│       ├── tailwind.css
│       └── tailwind.output.css
├── components
│   ├── HandlerImportScene
│   │   └── HandlerImportScene.tsx
│   ├── Fera
│   │   └── Fera.tsx
│   ├── NewScene
│   │   └── NewScene.tsx
│   └── Header
│       └── Header.tsx
├── config
│   ├── base.ts
│   ├── dev.ts
│   ├── index.ts
│   └── prod.ts
├── i18n
│   ├── en.json
│   ├── fr.json
│   └── index.ts
├── index.tsx
├── pages
│   └── Home
│       └── Home.tsx
├── react-app-env.d.ts
├── tests
│   ├── App.test.tsx
│   └── setupTests.ts
└── types
    └── react-app-env.d.ts

```

### Feature description

TBA

### How to contribute to this project? 

TBA

### How to use?

TBA

## NPM custom commands
`npm run`
- `start`: Start webpack in watch mode for development.
- `build`: Build for production.
- `test`: Run jest tests.
- `format`: Run prettier. 
- `lint`: Run eslint.

## Other/Optional considerations

https://www.geeksforgeeks.org/how-to-pass-data-from-one-component-to-other-component-in-reactjs/
https://www.geeksforgeeks.org/how-to-pass-data-from-child-component-to-its-parent-in-reactjs/
https://stackoverflow.com/questions/53920260/passing-state-between-components
https://redux.js.org/tutorials/fundamentals/part-5-ui-react
https://medium.com/how-to-react/different-ways-to-loop-through-arrays-and-objects-in-react-39bcd870ccf

## Notes

- setState function render the page each time you call it, so it's important to call it once in your method and defined every value in your state.
- 

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

*Bastien GUIHARD*

