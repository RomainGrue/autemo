--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.1

-- Started on 2022-03-07 11:04:17 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16385)
-- Name: action; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.action (
    id_action bigint NOT NULL,
    id_scene bigint NOT NULL,
    text text NOT NULL,
    estinterieur boolean NOT NULL
);


ALTER TABLE public.action OWNER TO root;

--
-- TOC entry 210 (class 1259 OID 16390)
-- Name: action_id_action_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.action ALTER COLUMN id_action ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.action_id_action_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 211 (class 1259 OID 16391)
-- Name: fait; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.fait (
    id_fait bigint NOT NULL,
    debut integer NOT NULL,
    fin integer NOT NULL,
    interieur boolean NOT NULL,
    id_scene bigint NOT NULL
);


ALTER TABLE public.fait OWNER TO root;

--
-- TOC entry 212 (class 1259 OID 16394)
-- Name: fait_id_fait_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.fait ALTER COLUMN id_fait ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fait_id_fait_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 213 (class 1259 OID 16395)
-- Name: participant; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.participant (
    id_participant bigint NOT NULL,
    id_scene bigint NOT NULL,
    role character varying(50)
);


ALTER TABLE public.participant OWNER TO root;

--
-- TOC entry 214 (class 1259 OID 16398)
-- Name: participant_id_participant_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.participant ALTER COLUMN id_participant ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.participant_id_participant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 215 (class 1259 OID 16399)
-- Name: phrase; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.phrase (
    id_phrase bigint NOT NULL,
    id_scene bigint NOT NULL,
    emotion smallint NOT NULL,
    i_debut integer NOT NULL,
    i_fin integer NOT NULL
);


ALTER TABLE public.phrase OWNER TO root;

--
-- TOC entry 216 (class 1259 OID 16402)
-- Name: phrase_id_phrase_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.phrase ALTER COLUMN id_phrase ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.phrase_id_phrase_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 16403)
-- Name: progression; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.progression (
    id_scene integer NOT NULL,
    segmentation smallint NOT NULL,
    emotions smallint NOT NULL,
    participants smallint NOT NULL,
    faits smallint NOT NULL,
    raisons smallint NOT NULL
);


ALTER TABLE public.progression OWNER TO root;

--
-- TOC entry 218 (class 1259 OID 16406)
-- Name: qualificateur; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.qualificateur (
    id_qualificateur bigint NOT NULL,
    id_participant bigint NOT NULL,
    debut smallint NOT NULL,
    fin smallint NOT NULL
);


ALTER TABLE public.qualificateur OWNER TO root;

--
-- TOC entry 219 (class 1259 OID 16409)
-- Name: qualificateur_id_qualificateur_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.qualificateur ALTER COLUMN id_qualificateur ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.qualificateur_id_qualificateur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 220 (class 1259 OID 16410)
-- Name: raison; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.raison (
    id_raison bigint NOT NULL,
    id_phrase bigint NOT NULL,
    debut smallint NOT NULL,
    fin smallint NOT NULL
);


ALTER TABLE public.raison OWNER TO root;

--
-- TOC entry 221 (class 1259 OID 16413)
-- Name: raison_id_raison_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.raison ALTER COLUMN id_raison ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.raison_id_raison_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 222 (class 1259 OID 16414)
-- Name: scene; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.scene (
    id_scene bigint NOT NULL,
    corrected boolean DEFAULT false,
    text text NOT NULL,
    titre character varying(50) NOT NULL,
    threshold smallint NOT NULL
);


ALTER TABLE public.scene OWNER TO root;

--
-- TOC entry 223 (class 1259 OID 16420)
-- Name: scene_id_scene_seq; Type: SEQUENCE; Schema: public; Owner: root
--

ALTER TABLE public.scene ALTER COLUMN id_scene ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.scene_id_scene_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3203 (class 2606 OID 16422)
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id_action);


--
-- TOC entry 3205 (class 2606 OID 16424)
-- Name: fait fait_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.fait
    ADD CONSTRAINT fait_pkey PRIMARY KEY (id_fait);


--
-- TOC entry 3207 (class 2606 OID 16426)
-- Name: participant participant_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT participant_pkey PRIMARY KEY (id_participant);


--
-- TOC entry 3209 (class 2606 OID 16428)
-- Name: phrase phrase_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.phrase
    ADD CONSTRAINT phrase_pkey PRIMARY KEY (id_phrase);


--
-- TOC entry 3211 (class 2606 OID 16430)
-- Name: progression progression_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.progression
    ADD CONSTRAINT progression_pkey PRIMARY KEY (id_scene);


--
-- TOC entry 3213 (class 2606 OID 16432)
-- Name: qualificateur qualificateur_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.qualificateur
    ADD CONSTRAINT qualificateur_pkey PRIMARY KEY (id_qualificateur);


--
-- TOC entry 3215 (class 2606 OID 16434)
-- Name: raison raison_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.raison
    ADD CONSTRAINT raison_pkey PRIMARY KEY (id_raison);


--
-- TOC entry 3217 (class 2606 OID 16436)
-- Name: scene scene_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT scene_pkey PRIMARY KEY (id_scene);


--
-- TOC entry 3218 (class 2606 OID 16437)
-- Name: action action_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_fk FOREIGN KEY (id_scene) REFERENCES public.scene(id_scene);


--
-- TOC entry 3219 (class 2606 OID 16442)
-- Name: fait fait_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.fait
    ADD CONSTRAINT fait_fk FOREIGN KEY (id_scene) REFERENCES public.scene(id_scene) NOT VALID;


--
-- TOC entry 3220 (class 2606 OID 16447)
-- Name: participant participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT participant_fk FOREIGN KEY (id_scene) REFERENCES public.scene(id_scene);


--
-- TOC entry 3221 (class 2606 OID 16452)
-- Name: phrase phrase_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.phrase
    ADD CONSTRAINT phrase_fk FOREIGN KEY (id_scene) REFERENCES public.scene(id_scene);


--
-- TOC entry 3222 (class 2606 OID 16457)
-- Name: progression progression_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.progression
    ADD CONSTRAINT progression_fk FOREIGN KEY (id_scene) REFERENCES public.scene(id_scene) NOT VALID;


--
-- TOC entry 3223 (class 2606 OID 16462)
-- Name: qualificateur qualificateur_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.qualificateur
    ADD CONSTRAINT qualificateur_fk FOREIGN KEY (id_participant) REFERENCES public.participant(id_participant);


--
-- TOC entry 3224 (class 2606 OID 16467)
-- Name: raison raison_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.raison
    ADD CONSTRAINT raison_fk FOREIGN KEY (id_phrase) REFERENCES public.phrase(id_phrase);


-- Completed on 2022-03-07 11:04:17 UTC

--
-- PostgreSQL database dump complete
--

