# Autemo
## Getting started
Start by building the backend using the following command:
```bash
cd back_end
./mvnw clean package
```
Then use `docker-compose` to run the application:
```bash
# cd ../ if you are still in the backend dir
# use --build option for the first run
sudo docker-compose up --build
```
**Note** : Every modification on the front will not be updated.

If you modification in real time on front_end, you need to launch respectively :

In front_end/ package :
```
npm run start
```

In back_end/ package :
```
./mvnw quarkus:dev
```
## Testing API using cURL
Run a manual test using the following command:
```bash
curl -X POST http://localhost:8080/API/analyse -H 'Content-Type: application/json' -d '{"scene": "your_scene_here"}'
```
or using postman...

Please mind that the request body may change overtime.

## Testing frontend

Run in front_end package :
```
npm run start
```
open your web browser using the following URI: `localhost:3000`

### Check OpenApi doc

open your web browser using the following URI : https://editor.swagger.io/

Copy paste the file /front_end/openapi.yml to see details about routes

## Using python scripts in standalone

- TBA 
```bash
pip3 install stanza
pip3 install transformers
```

## Running tests
### Backend
```bash
./mvnw test
```
### Frontend
 
## Troubleshooting
### Backend
- Make sure that `JAVA_HOME` is setup correctly
- Check `application.properties` for CORS related problems
- Process builder `python3` command might cause problems in dev given your environement 
- Error `listen tcp4 0.0.0.0:5432: bind: address already in use` while docker-compose up
  - If `lsof -i :5432` doesn't show you any output, you can use `sudo ss -lptn 'sport = :5432'` to see what process is bound to the port.
  Proceed further with `kill <pid>`

### Frontend
- For testing make sure that modifications are updated everytime you save the file

### Docker-compose
- For Mac : Docker might be broken in brew packages, we recommand installing Docker-desktop to avoid installation trouble (https://docs.docker.com/desktop/mac/install/) 
- Elasticsearch might crash with `error 137` if docker-desktop memory is lower than 4gb
- In order to trigger DB initialization use the command `docker-compose down --volumes` before relaunching 
