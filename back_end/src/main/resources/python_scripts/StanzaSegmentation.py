import stanza
import sys
import json

if __name__ == '__main__':

    if len(sys.argv) != 2:
        for arg in sys.argv:
            print(arg)
        raise ValueError('Wrong number of arguments : ' + str(len(sys.argv)))

    stanza.download('fr')
    nlp = stanza.Pipeline('fr')
    scene = sys.argv[1]
    doc = nlp(scene)
    sentences = list()
    entities = list()

    for sentence in doc.sentences:
        temp = dict()
        temp["text"] = sentence.text
        temp["start_char"] = sentence.words[0].start_char
        temp["end_char"] = sentence.words[len(sentence.words)-1].end_char
        if(temp["start_char"]==None):
            temp["start_char"] = temp["end_char"]-len(temp["text"])
        sentences.append(temp)

    for entity in doc.entities:
        entities.append(entity.to_dict())

    try:
        outfile = json.dumps(sentences, indent=4)
        print(outfile + '|')
        print(json.dumps(entities, indent=4))
    except Exception as e:
        print(e)