import sys

from transformers import pipeline
import json

LABELS_ID = {
    "neutre":0,
    "colère":1,
    "peur":2,
    "tristesse":3,
    "joie":4
}

def get_emotions_from_sentence(emotions, score):
    emo = 0
    max_index = score.index(max(score))
    if score[max_index] > THRESHOLD:
        emo = LABELS_ID[emotions[max_index]]
    return emo

if __name__ == '__main__':
    classifier_fr = pipeline("zero-shot-classification", model="BaptisteDoyen/camembert-base-xnli")
    labels = ["colère", "peur", "tristesse", "joie"]
    THRESHOLD = float(sys.argv[1])
    txt = sys.argv[2:]
    emotions = list()

    hypothesis_template = "L'émotion de ce texte est la {}."
    for struct in txt:
        json_phrase = json.loads(struct)
        res = classifier_fr(json_phrase["sentence"], candidate_labels=labels, hypothesis_template=hypothesis_template)
        json_phrase["emotion"] = get_emotions_from_sentence(res["labels"], res["scores"])
        emotions.append(json_phrase)

    try:
        print(json.dumps(emotions, indent=4))
    except Exception as e:
        print(e)
