package fr.uge.autemo.stateupdate.pojo.payload;

public class StateUpdatePayload {
    public Long id;
    public int segmentation;
    public int participants;
    public int emotions;
    public int faits;
    public int raisons;
    public String error;

    public StateUpdatePayload(Long id,
                              int segmentation,
                              int participants,
                              int emotions,
                              int faits,
                              int raisons,
                              String error) {
        this.id = id;
        this.segmentation = segmentation;
        this.participants = participants;
        this.emotions = emotions;
        this.faits = faits;
        this.raisons = raisons;
        this.error = error;
    }
}
