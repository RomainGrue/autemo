package fr.uge.autemo.stateupdate;

import fr.uge.autemo.analysis.entities.SceneEntity;
import fr.uge.autemo.analysis.pojo.payload.FERA;
import fr.uge.autemo.stateupdate.entities.ProgressionEntity;
import fr.uge.autemo.stateupdate.pojo.payload.StateUpdatePayload;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/API/analysis-state")
public class StateUpdateResource {
    @Inject
    EntityManager em;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public StateUpdatePayload get(@PathParam("id") Long id) {
        try {
            ProgressionEntity p = (ProgressionEntity) em.createQuery("SELECT p from ProgressionEntity p where p = " + id).getSingleResult();
            return new StateUpdatePayload(id, p.getSegmentation(), p.getParticipants(), p.getEmotions(), p.getFaits(), p.getRaisons(), null);
        } catch (NoResultException nre) {
            return new StateUpdatePayload(id, 0, 0, 0, 0, 0, "Not found: Analysis id not found");
        }
    }
}
