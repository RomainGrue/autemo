package fr.uge.autemo.stateupdate.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;

@Entity
@Table(name = "progression")
public class ProgressionEntity /*extends PanacheEntity*/ {
    @Id
    @Column(name = "id_scene")
    private Long idScene;

    @Column(name = "segmentation")
    private int segmentation;

    @Column(name = "emotions")
    private int emotions;

    @Column(name = "participants")
    private int participants;

    @Column(name = "faits")
    private int faits;

    @Column(name = "raisons")
    private int raisons;

    public int getFaits() {
        return faits;
    }

    public void setFaits(int faits) {
        this.faits = faits;
    }

    public int getRaisons() {
        return raisons;
    }

    public void setRaisons(int raisons) {
        this.raisons = raisons;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    public int getSegmentation() {
        return this.segmentation;
    }

    public void setSegmentation(int segmentation) {
        this.segmentation = segmentation;
    }

    public int getEmotions() {
        return this.emotions;
    }

    public void setEmotions(int emotions) {
        this.emotions = emotions;
    }

    public int getParticipants() {
        return this.participants;
    }

    public void setParticipants(int participants) {
        this.participants = participants;
    }
}
