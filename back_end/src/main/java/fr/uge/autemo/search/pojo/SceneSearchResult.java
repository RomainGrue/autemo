package fr.uge.autemo.search.pojo;

public class SceneSearchResult {
    public Long id;
    public String titre;
    public String sceneSnippet;

    public SceneSearchResult(Long id,String titre, String sceneSnippet) {
        this.id = id;
        this.titre = titre;
        this.sceneSnippet = sceneSnippet;
    }
}
