package fr.uge.autemo.search;

import fr.uge.autemo.analysis.entities.SceneEntity;
import fr.uge.autemo.analysis.pojo.payload.FERA;
import fr.uge.autemo.search.pojo.SceneSearchResult;
import io.github.cdimascio.dotenv.Dotenv;
import org.hibernate.search.engine.search.predicate.dsl.SearchPredicateFactory;
import org.hibernate.search.mapper.orm.session.SearchSession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/API/search")
@ApplicationScoped
public class SearchResource {
    @Inject
    SearchSession searchSession;

    @GET
    @Path("/{text}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public List<SceneSearchResult> get(@PathParam("text") String text) {
        var maxLength = 100;
        var scenes = searchSession.search(SceneEntity.class)
                .where(f -> text == null || text.trim().isEmpty() ?
                        f.matchAll() :
                        //f.simpleQueryString().fields("titre").matching(text)
                        f.bool().should(q1 -> q1.match()
                                        .field("titre")
                                        .matching(text)
                                        .boost(2.0f))
                                .should(q2 -> q2.match()
                                        .field("text")
                                        .matching(text))
                )
                .fetchHits(5);

        return scenes
                .stream()
                .map(s -> new SceneSearchResult(s.getIdScene(), s.getTitre(), s.getText().substring(0, Math.min(50, s.getText().length())) + "..."))
                .collect(Collectors.toList());
    }
}