package fr.uge.autemo.search.elasticsearchconfig;

import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurationContext;
import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurer;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Dependent
@Named("mySearchConfigurer")
public class TitleSearchConfigurer implements ElasticsearchAnalysisConfigurer {

    @Override
    public void configure(ElasticsearchAnalysisConfigurationContext context) {
        context.analyzer("french").custom()
                .tokenizer("standard")
                .tokenFilters("asciifolding", "lowercase");
    }
}