
package fr.uge.autemo.analysis;

import fr.uge.autemo.analysis.entities.SceneEntity;
import fr.uge.autemo.analysis.nlp.AnalysisContext;
import fr.uge.autemo.analysis.nlp.emotionextractors.ZeroShotCamemBERTEmotionExtracteur;
import fr.uge.autemo.analysis.nlp.participantextractors.NERSpacyParticipantExtracteur;
import fr.uge.autemo.analysis.nlp.segmentation.CoreNLPSegmentation;
import fr.uge.autemo.analysis.pojo.*;
import fr.uge.autemo.analysis.pojo.body.SceneText;
import fr.uge.autemo.analysis.pojo.payload.AnalysisPayload;
import fr.uge.autemo.analysis.pojo.payload.FERA;
import io.vertx.mutiny.core.eventbus.EventBus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Path("/API/analyse")
@ApplicationScoped
public class AnalysisResource {

    @Inject
    EventBus bus;

    @Inject
    @PersistenceContext
    EntityManager em;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public FERA get(@PathParam("id") Long id) {
        if (id < 0) {
            return new FERA(id, -1, null, null, null, null, null, "Malformed body: id cannot be negative");
        }
        SceneEntity sceneEntity = em.find(SceneEntity.class, id);
        if (sceneEntity == null) {
            return new FERA(id, -1, null, null, null, null, null, "Not found: id does not exist");
        }

        var phrases = new ArrayList<Phrase>();
        for (var phrase : sceneEntity.getPhraseEntityList()) {
            var raisons = phrase.getRaisonEntityList().stream().map(raisonEntity -> {
                return new Raison(raisonEntity.getIdRaison(), raisonEntity.getDebut(), raisonEntity.getFin());
            }).collect(Collectors.toList());
            phrases.add(new Phrase(phrase.getIdPhrase(), phrase.getDebut(), phrase.getFin(), phrase.getEmotion(), raisons));

        }

        var participants = new ArrayList<Participant>();
        for (var participant : sceneEntity.getParticipantEntityList()) {
            var qualifiers = new ArrayList<Qualificateur>();
            for (var qualifier : participant.getQualificateurEntityList()) {
                qualifiers.add(new Qualificateur(qualifier.getIdQualificateur(), qualifier.getDebut(), qualifier.getFin()));
            }
            participants.add(new Participant(participant.getIdParticipant(), qualifiers, participant.getRole()));
        }

        var faits = new ArrayList<Fait>();
        for (var f : sceneEntity.getFaitEntityList()) {
            faits.add(new Fait(f.getIdFait(), f.getInterieur(), f.getDebut(), f.getFin()));
        }

        var actions = new ArrayList<Action>();
        var actionEntities = sceneEntity.getActionEntityList();
        for (var a : actionEntities) {
            actions.add(new Action(a.getIdAction(), a.getText(), a.isEstInterieur()));
        }

        //todo get actions
        return new FERA(id, sceneEntity.getThreshold(), participants, phrases, faits, actions, sceneEntity.getText(), null);
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AnalysisPayload post(SceneText userScene) {
        if (userScene.getScene().isBlank()) {
            return new AnalysisPayload("", "", 0L, 0.01, "Malformed body: empty scene");
        }
        if (SceneEntity.titleExist(userScene.getTitre(), em)){
            return new AnalysisPayload("", "", 0L, 0.01, "Le titre " + userScene.getTitre() + " existe déjà !");
        }
        var sceneEntity = new SceneEntity();
        sceneEntity.setCorrected(false);
        sceneEntity.setText(userScene.getScene());
        sceneEntity.setTitre(userScene.getTitre());
        sceneEntity.setThreshold(userScene.getThreshold());
        em.persist(sceneEntity);
        em.flush();
        var id = sceneEntity.getIdScene();
        System.out.println("THRESHOLD : ");
        System.out.println(userScene.getThreshold());
        var ctx = new AnalysisContext.AnalysisContextBuilder()
                .setId(id)
                .setScene(userScene.getScene())
                .setSegmentationEntityStrategy(new CoreNLPSegmentation())
                .setEmotionExtractionStrategy(new ZeroShotCamemBERTEmotionExtracteur(userScene.getThreshold()))
                .setParticipantExtractionStrategy(new NERSpacyParticipantExtracteur())
                .setEm(em)
                .setQueueRef(bus)
                .build();
        try {
            var payload = new AnalysisPayload(userScene.getTitre(), userScene.getScene(), id, userScene.getThreshold(), null);
            bus.send("analysis", ctx);
            return payload;
        } catch (IllegalArgumentException iae) {
            return new AnalysisPayload("", "", 0L, 0.01, iae.getMessage());
        }

    }
}