package fr.uge.autemo.analysis.pojo;

import java.util.List;
import java.util.Objects;

/**
 */
public final class Participant {
    public List<Qualificateur> qualificateur;
    public String role;
    public Long id;

    /**
     * @param qualificateur list of words to qualify a participant
     * @param role the role of the participant
     */
    public Participant(Long id, List<Qualificateur> qualificateur, String role) {
        this.qualificateur = qualificateur;
        this.role = role;
        this.id = id;
    }

    public List<Qualificateur> qualificateur() {
        return qualificateur;
    }

    public String role() {
        return role;
    }

}
