package fr.uge.autemo.analysis.nlp;

import fr.uge.autemo.analysis.pojo.Fait;
import fr.uge.autemo.analysis.pojo.Phrase;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;

@FunctionalInterface
public interface IFactExtractor {
    ArrayList<Fait> execute() throws IOException, InterruptedException, ParseException;
}
