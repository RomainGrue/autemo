package fr.uge.autemo.analysis.nlp;

import fr.uge.autemo.analysis.pojo.Phrase;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@FunctionalInterface
public interface IEmotionExtractor {
    ArrayList<Phrase> execute(List<Phrase> phrases, String scene) throws IOException, InterruptedException, ParseException;
}
