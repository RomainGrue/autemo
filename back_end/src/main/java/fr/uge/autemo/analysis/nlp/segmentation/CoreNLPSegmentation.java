package fr.uge.autemo.analysis.nlp.segmentation;

import fr.uge.autemo.analysis.nlp.ISegmentationEntityExtractor;
import io.github.cdimascio.dotenv.Dotenv;
import org.eclipse.microprofile.config.ConfigProvider;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CoreNLPSegmentation implements ISegmentationEntityExtractor {

    public HashMap<String, String> execute(String sceneText) throws IOException, InterruptedException, ParseException {
        ArrayList<String> cmd = new ArrayList<>();
        String config = ConfigProvider.getConfig().getValue("environment", String.class);
        if (config.equals("prod")){
            cmd.add("python3"); cmd.add("/deployments/python_scripts/StanzaSegmentation.py");
        }else{
            cmd.add("python3"); cmd.add("src/main/resources/python_scripts/StanzaSegmentation.py");
        }
        cmd.add(sceneText);
        System.out.println("P : "+ Arrays.toString(cmd.toArray(new String[cmd.size()])));
        ProcessBuilder pb = new ProcessBuilder(cmd.toArray(new String[cmd.size()]))
                .inheritIO()
                .redirectOutput(ProcessBuilder.Redirect.PIPE);
        var process = pb.start();
        System.out.println("pythonReturnValue : " +process.waitFor());
        System.out.println("P FINISHED : "+ Arrays.toString(cmd.toArray(new String[cmd.size()])));
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();
        System.out.println("RESULT : " + result);

        var res = result.split("\\|");
        System.out.println("RES : " + Arrays.toString(res));
        var segmentation = res[0];
        var entities = res[1];
        var returnObject = new HashMap<String, String>();
        returnObject.put("segmentation", segmentation);
        returnObject.put("entities", entities);
        return returnObject;
    }

}
