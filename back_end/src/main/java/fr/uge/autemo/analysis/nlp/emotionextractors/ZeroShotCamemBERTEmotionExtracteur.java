package fr.uge.autemo.analysis.nlp.emotionextractors;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import fr.uge.autemo.analysis.nlp.IEmotionExtractor;
import fr.uge.autemo.analysis.pojo.Phrase;
import io.github.cdimascio.dotenv.Dotenv;
import org.eclipse.microprofile.config.ConfigProvider;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ZeroShotCamemBERTEmotionExtracteur implements IEmotionExtractor{

    private double threshold;

    public ZeroShotCamemBERTEmotionExtracteur(double threshold) {
        this.threshold = threshold;
    }

    /**
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     */
    @Override
    public ArrayList<Phrase> execute(List<Phrase> phrases, String scene) throws IOException, InterruptedException, ParseException {
        ArrayList<String> cmd = new ArrayList<>();
        cmd.add("python3"); cmd.add("/deployments/python_scripts/TransformersEmotion.py");
        cmd.add(Double.toString(threshold));
//      cmd.add("python3"); cmd.add("src/main/resources/python_scripts/TransformersEmotion.py");
        HashMap<String, String> temp = new HashMap<>();
        Gson gson = new Gson();
        phrases.forEach(p -> {
            temp.put("sentence",scene.substring(p.debut, p.fin));
            temp.put("debut", Integer.toString(p.debut));
            temp.put("fin", Integer.toString(p.fin));
            temp.put("emotion", Integer.toString(-1));
            cmd.add(gson.toJson(temp));
        });

        ProcessBuilder pb = new ProcessBuilder(cmd.toArray(new String[cmd.size()]))
                .inheritIO()
                .redirectOutput(ProcessBuilder.Redirect.PIPE);
        var process = pb.start();
        process.waitFor();


        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();

        System.out.println(result);

        ArrayList<LinkedTreeMap<?, ?>> s = gson.fromJson(result, ArrayList.class);
        ArrayList<Phrase> ret = new ArrayList<>();
        s.forEach(e -> ret.add(new Phrase(null, Integer.parseInt((String) e.get("debut")), Integer.parseInt((String) e.get("fin")), ((Double) e.get("emotion")).intValue(), null)));
        System.out.println(ret);
        return ret;
    }

}
