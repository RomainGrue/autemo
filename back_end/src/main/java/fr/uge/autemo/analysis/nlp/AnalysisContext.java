package fr.uge.autemo.analysis.nlp;

import io.vertx.mutiny.core.eventbus.EventBus;

import javax.persistence.EntityManager;
import java.util.Objects;

public class AnalysisContext {

    private final Long id;
    private final String scene;
    private final IEmotionExtractor emotionExtractionStrategy;
    private final IParticipantExtractor participantExtractionStrategy;
    private final IFactExtractor factExtractorStrategy;
    private final IReasonExtractor reasonExtractorStrategy;
    private final ISegmentationEntityExtractor segmentationEntityStrategy;
    private final EntityManager em;
    private final EventBus queueRef;

    public AnalysisContext(AnalysisContextBuilder builder) {
        this.id = builder.id;
        this.scene = builder.scene;
        this.emotionExtractionStrategy = builder.emotionExtractionStrategy;
        this.participantExtractionStrategy = builder.participantExtractionStrategy;
        this.segmentationEntityStrategy = builder.segmentationEntityStrategy;
        this.em = builder.em;
        this.queueRef = builder.queueRef;
        this.factExtractorStrategy = builder.factExtractorStrategy;
        this.reasonExtractorStrategy = builder.reasonExtractorStrategy;
    }

    public IFactExtractor getFactExtractorStrategy() {
        return factExtractorStrategy;
    }

    public IReasonExtractor getReasonExtractorStrategy() {
        return reasonExtractorStrategy;
    }

    public EventBus getQueueRef() {
        return queueRef;
    }

    public EntityManager getEm() {
        return em;
    }

    public Long getId() {
        return id;
    }

    public String getScene() {
        return scene;
    }

    public IEmotionExtractor getEmotionExtractionStrategy() {
        return emotionExtractionStrategy;
    }

    public IParticipantExtractor getParticipantExtractionStrategy() {
        return participantExtractionStrategy;
    }

    public ISegmentationEntityExtractor getSegmentationEntityStrategy() {
        return segmentationEntityStrategy;
    }

    public static class AnalysisContextBuilder {
        private Long id;
        private String scene;
        private IEmotionExtractor emotionExtractionStrategy;
        private IParticipantExtractor participantExtractionStrategy;
        private ISegmentationEntityExtractor segmentationEntityStrategy;
        private IFactExtractor factExtractorStrategy;
        private IReasonExtractor reasonExtractorStrategy;
        private EntityManager em;
        private EventBus queueRef;

        public AnalysisContextBuilder setFactExtractorStrategy(IFactExtractor factExtractorStrategy) {
            Objects.requireNonNull(factExtractorStrategy);
            this.factExtractorStrategy = factExtractorStrategy;
            return this;
        }

        public AnalysisContextBuilder setReasonExtractorStrategy(IReasonExtractor reasonExtractorStrategy) {
            Objects.requireNonNull(reasonExtractorStrategy);
            this.reasonExtractorStrategy = reasonExtractorStrategy;
            return this;
        }

        public AnalysisContextBuilder setQueueRef(EventBus queueRef) {
            this.queueRef = queueRef;
            return this;
        }

        public AnalysisContextBuilder setEm(EntityManager em) {
            this.em = em;
            return this;
        }

        public AnalysisContextBuilder setId(Long id) {
            if (id < 0) {
                throw new IllegalArgumentException("id cannot be negative");
            }
            this.id = id;
            return this;
        }

        public AnalysisContextBuilder setScene(String scene) {
            Objects.requireNonNull(scene);
            if (scene.isBlank()) {
                throw new IllegalArgumentException("blank scene");
            }
            this.scene = scene;
            return this;
        }

        public AnalysisContextBuilder setEmotionExtractionStrategy(IEmotionExtractor emotionExtractionStrategy) {
            Objects.requireNonNull(emotionExtractionStrategy);
            this.emotionExtractionStrategy = emotionExtractionStrategy;
            return this;
        }

        public AnalysisContextBuilder setParticipantExtractionStrategy(IParticipantExtractor participantExtractionStrategy) {
            Objects.requireNonNull(participantExtractionStrategy);
            this.participantExtractionStrategy = participantExtractionStrategy;
            return this;
        }

        public AnalysisContextBuilder setSegmentationEntityStrategy(ISegmentationEntityExtractor segmentationEntityStrategy) {
            Objects.requireNonNull(segmentationEntityStrategy);
            this.segmentationEntityStrategy = segmentationEntityStrategy;
            return this;
        }

        public AnalysisContext build() {
            if (this.scene == null
                    || this.id == 0
                    || this.segmentationEntityStrategy == null
                    || this.queueRef == null
            ) {
                throw new IllegalStateException("Malformed context");
            }
            return new AnalysisContext(this);
        }
    }
}
