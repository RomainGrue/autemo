package fr.uge.autemo.analysis.entities;

import javax.persistence.*;

@Entity
@Table(name = "fait")
public class FaitEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_fait")
    private Long idFait;

    @Column(name = "debut")
    private int debut;

    @Column(name = "fin")
    private int fin;

    @Column(name = "interieur")
    private Boolean interieur;

    @Column(name = "id_scene")
    private Long idScene;

    public Long getIdFait() {
        return this.idFait;
    }

    public void setIdFait(Long idFait) {
        this.idFait = idFait;
    }

    public int getDebut() {
        return this.debut;
    }

    public void setDebut(int debut) {
        this.debut = debut;
    }

    public int getFin() {
        return this.fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    public Boolean getInterieur() {
        return this.interieur;
    }

    public void setInterieur(Boolean interieur) {
        this.interieur = interieur;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    @Override
    public String toString() {
        return "FaitEntity{" +
                "idFait=" + idFait +
                ", debut=" + debut +
                ", fin=" + fin +
                ", interieur=" + interieur +
                ", idScene=" + idScene +
                '}';
    }
}
