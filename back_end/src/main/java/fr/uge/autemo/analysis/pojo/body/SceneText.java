package fr.uge.autemo.analysis.pojo.body;

public class SceneText {
    String scene;
    String titre;
    double threshold;

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public SceneText() {
    }

    public SceneText(String scene) {
        this.scene = scene;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double getThreshold() {
        return threshold;
    }
}

