package fr.uge.autemo.analysis.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "phrase")
public class PhraseEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_phrase")
    private Long idPhrase;

    @Column(name = "id_scene")
    private Long idScene;

    @Column(name = "emotion")
    private int emotion;

    @Column(name = "i_debut")
    private int debut;

    @Column(name = "i_fin")
    private int fin;

    @OneToMany
    @JoinColumn(name = "id_phrase")
    private List<RaisonEntity> raisonEntityList;

    public List<RaisonEntity> getRaisonEntityList() {
        return raisonEntityList;
    }

    public void setRaisonEntityList(List<RaisonEntity> raisonEntityList) {
        this.raisonEntityList = raisonEntityList;
    }

    public Long getIdPhrase() {
        return this.idPhrase;
    }

    public void setIdPhrase(Long idPhrase) {
        this.idPhrase = idPhrase;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    public int getEmotion() {
        return this.emotion;
    }

    public void setEmotion(int emotion) {
        this.emotion = emotion;
    }

    public int getDebut() {
        return debut;
    }

    public void setDebut(int debut) {
        this.debut = debut;
    }

    public int getFin() {
        return fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "PhraseEntity{" +
                "idPhrase=" + idPhrase +
                ", idScene=" + idScene +
                ", emotion=" + emotion +
                ", debut=" + debut +
                ", fin=" + fin +
                '}';
    }
}
