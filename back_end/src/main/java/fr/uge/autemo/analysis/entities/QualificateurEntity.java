package fr.uge.autemo.analysis.entities;

import javax.persistence.*;

@Entity
@Table(name = "qualificateur")
public class QualificateurEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_qualificateur")
    private Long idQualificateur;

    @Column(name = "id_participant")
    private Long idParticipant;

    @Column(name = "debut")
    private int debut;

    @Column(name = "fin")
    private int fin;

    public Long getIdQualificateur() {
        return this.idQualificateur;
    }

    public void setIdQualificateur(Long idQualificateur) {
        this.idQualificateur = idQualificateur;
    }

    public Long getIdParticipant() {
        return this.idParticipant;
    }

    public void setIdParticipant(Long idParticipant) {
        this.idParticipant = idParticipant;
    }

    public int getDebut() {
        return this.debut;
    }

    public void setDebut(int debut) {
        this.debut = debut;
    }

    public int getFin() {
        return this.fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "QualificateurEntity{" +
                "idQualificateur=" + idQualificateur +
                ", idParticipant=" + idParticipant +
                ", debut=" + debut +
                ", fin=" + fin +
                '}';
    }
}
