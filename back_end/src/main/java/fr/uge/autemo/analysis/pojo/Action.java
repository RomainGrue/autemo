package fr.uge.autemo.analysis.pojo;

public class Action {
    public Long id;
    public String text;
    public boolean estinterieur;

    public Action(Long id, String text, boolean estinterieur) {
        this.id = id;
        this.text = text;
        this.estinterieur = estinterieur;
    }
}
