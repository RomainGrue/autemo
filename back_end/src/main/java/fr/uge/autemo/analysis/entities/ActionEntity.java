package fr.uge.autemo.analysis.entities;

import javax.persistence.*;

@Entity
@Table(name = "action")
public class ActionEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_action")
    private Long idAction;

    @Column(name = "id_scene")
    private Long idScene;

    @Column(name = "text")
    private String text;

    @Column(name = "estinterieur")
    private boolean estinterieur;

    public Long getIdAction() {
        return this.idAction;
    }

    public void setIdAction(Long idAction) {
        this.idAction = idAction;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isEstInterieur() {
        return estinterieur;
    }

    public void setEstInterieur(boolean estInterieur) {
        this.estinterieur = estInterieur;
    }

    @Override
    public String toString() {
        return "ActionEntity{" +
                "idAction=" + idAction +
                ", idScene=" + idScene +
                ", text='" + text + '\'' +
                ", estinterieur=" + estinterieur +
                '}';
    }
}
