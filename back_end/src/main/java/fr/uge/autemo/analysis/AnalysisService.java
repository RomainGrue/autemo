package fr.uge.autemo.analysis;

import fr.uge.autemo.analysis.entities.*;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import fr.uge.autemo.analysis.nlp.AnalysisContext;
import fr.uge.autemo.analysis.pojo.*;
import fr.uge.autemo.analysis.pojo.payload.FERA;
import fr.uge.autemo.stateupdate.entities.ProgressionEntity;
import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import org.json.simple.parser.ParseException;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.stream.Collectors;

public class AnalysisService {
    private static class State {
        public final ProgressionEntity pe;
        public final AnalysisContext ctx;
        public HashMap<String, String> map;
        public ArrayList<Phrase> phrases;

        public State(ProgressionEntity pe, AnalysisContext ctx) {
            this.pe = pe;
            this.ctx = ctx;
        }

        void setHashMap(HashMap<String, String> hm) {
            this.map = hm;
        }
        void setArrayList(ArrayList<Phrase> al) {
            this.phrases = al;
        }

        ArrayList<Phrase> getPhrases() {
            if(Objects.isNull(phrases)) {
                ArrayList<LinkedTreeMap<?, ?>> tmp = new Gson().fromJson(map.get("segmentation"), ArrayList.class);
                return new ArrayList<>(tmp
                        .stream()
                        .map(s -> new Phrase(null, ((Double)s.get("start_char")).intValue(), ((Double)s.get("end_char")).intValue(), -1, null)).collect(Collectors.toList()));
            }
            return phrases;
        }
        ArrayList<LinkedTreeMap<?, ?>> getEntitiesFromHashMap() {
            return new Gson().fromJson(map.get("entities"), ArrayList.class);
        }
    }

    @ConsumeEvent("update-state")
    @Blocking
    @Transactional
    public void updateJobState(State state) {
        state.ctx.getEm().merge(state.pe);
    }

    @ConsumeEvent("extract-phrases")
    @Blocking
    @Transactional
    public void phraseExtraction(State state) {
        ArrayList<Phrase> phrases = null;
        try {
            var phrasesToAnalyse = state.getPhrases();
            phrases = state.ctx.getEmotionExtractionStrategy().execute(phrasesToAnalyse, state.ctx.getScene());
        } catch (IOException e) {
            state.pe.setEmotions(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException e) {
            state.pe.setEmotions(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new Error(e);
        }

        var em = state.ctx.getEm();
        var ctx = state.ctx;
        var i = 0;

        ArrayList<Phrase> phrasesWithId = new ArrayList<>();

        for (var p : phrases) {
            var pe = new PhraseEntity();
            pe.setIdScene(ctx.getId());
            pe.setEmotion(p.emotion);
            pe.setDebut(p.debut);
            pe.setFin(p.fin);
            em.persist(pe);
            em.flush();
            phrasesWithId.add(new Phrase(pe.getIdPhrase(), pe.getDebut(), pe.getFin(), pe.getEmotion(), null));
            i++;
        }

        state.setArrayList(phrasesWithId);

        state.pe.setEmotions(1);
        state.ctx.getQueueRef().send("update-state", state);
        System.out.println("Emotion analyse done");

        if(Objects.isNull(ctx.getReasonExtractorStrategy())) {
            state.pe.setRaisons(-1);
            ctx.getQueueRef().send("update-state", state);
        } else {
            ctx.getQueueRef().send("extract-reasons", state);
        }
    }

    @ConsumeEvent("extract-reasons")
    @Blocking
    @Transactional
    public void reasonExtraction(State state) {
        ArrayList<Raison> raisons = null;
        var em = state.ctx.getEm();
        var ctx = state.ctx;
        try {
            for (var p : state.getPhrases()){
                raisons = state.ctx.getReasonExtractorStrategy().execute(ctx.getScene(), p);
                raisons.forEach(raison -> {
                    var re = new RaisonEntity();
                    re.setIdPhrase(p.id);
                    re.setDebut(raison.debut);
                    re.setFin(raison.fin);
                    em.persist(re);
                    em.flush();
                });
            }
        } catch (IOException e) {
            state.pe.setRaisons(-1);
            state.ctx.getQueueRef().send("update-state", state);
            System.out.println("reason extraction failed");
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException | NullPointerException e) {
            state.pe.setRaisons(-1);
            state.ctx.getQueueRef().send("update-state", state);
            System.out.println("reason extraction failed");
        }

        state.pe.setRaisons(1);
        state.ctx.getQueueRef().send("update-state", state);
        System.out.println("reason analysis done");
    }

    @ConsumeEvent("extract-participants")
    @Blocking
    @Transactional
    public void participantExtraction(State state) {
        ArrayList<Participant> participants = null;
        try {
            participants = state.ctx.getParticipantExtractionStrategy().execute(state.getEntitiesFromHashMap());
        } catch (IOException e) {
            state.pe.setParticipants(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException e) {
            state.pe.setParticipants(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new Error(e);
        }
        var em = state.ctx.getEm();
        var ctx = state.ctx;

        for (var p : participants) {
            var pe = new ParticipantEntity();
            pe.setIdScene(ctx.getId());
            em.persist(pe);
            em.flush();
            var participantId = pe.getIdParticipant();
            for (var qualifier : p.qualificateur()) {
                var qe = new QualificateurEntity();
                qe.setIdParticipant(participantId);
                qe.setDebut(qualifier.debut);
                qe.setFin(qualifier.fin);
                em.persist(qe);
                em.flush();
            }
        }

        state.pe.setParticipants(1);
        state.ctx.getQueueRef().send("update-state", state);
        System.out.println("Participants analyse done");

    }

    @ConsumeEvent("extract-facts")
    @Blocking
    @Transactional
    public void factExtraction(State state) {
        ArrayList<Fait> faits = null;
        try {
            faits = state.ctx.getFactExtractorStrategy().execute();
        } catch (IOException e) {
            state.pe.setFaits(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException | NullPointerException e) {
            state.pe.setFaits(-1);
            state.ctx.getQueueRef().send("update-state", state);
            System.out.println("fact extraction failed");
        }
        var em = state.ctx.getEm();
        var ctx = state.ctx;

        for (var f : faits) {
            var fe = new FaitEntity();
            fe.setIdFait(f.id);
            fe.setDebut(f.debut);
            fe.setFin(f.fin);
            fe.setInterieur(f.interieur);
            fe.setIdScene(ctx.getId());
            em.persist(fe);
            em.flush();
        }

        state.pe.setFaits(1);
        state.ctx.getQueueRef().send("update-state", state);
        System.out.println("Participants analyse done");

    }
    
    @ConsumeEvent("analysis")
    @Blocking
    @Transactional
    public void analyse(AnalysisContext ctx) throws IOException {
        System.out.println("+++++++started on id: " + ctx.getId() + "+++++++");
        // init progression state
        var progressionEntity = new ProgressionEntity();
        progressionEntity.setEmotions(0);
        progressionEntity.setParticipants(0);
        progressionEntity.setSegmentation(0);
        progressionEntity.setFaits(0);
        progressionEntity.setRaisons(0);
        progressionEntity.setIdScene(ctx.getId());
        var state = new State(progressionEntity, ctx);
        ctx.getQueueRef().send("update-state", state);
        try {
            if(Objects.isNull(ctx.getSegmentationEntityStrategy())){
//                var segmented = ctx.getSegmentationStrategy()
//                        .execute(ctx.getScene())
//                        .stream()
//                        .map(s -> new Phrase(null, 0, 0, -1, null))
//                        .collect(Collectors.toList());
//                state.setArrayList(new ArrayList<>(segmented));
                throw new IllegalStateException(); //todo : thin about the right thing to do
            } else {
                state.setHashMap(ctx.getSegmentationEntityStrategy().execute(ctx.getScene()));
            }
        } catch (IOException e) {
            state.pe.setEmotions(-1);
            state.pe.setSegmentation(-1);
            state.pe.setParticipants(-1);
            state.pe.setFaits(-1);
            state.pe.setRaisons(-1);
            state.ctx.getQueueRef().send("update-state", state);
            return;

        } catch (ParseException | InterruptedException e) {
            state.pe.setEmotions(-1);
            state.pe.setSegmentation(-1);
            state.pe.setParticipants(-1);
            state.ctx.getQueueRef().send("update-state", state);
            e.printStackTrace();
            throw new Error(e);
        }

        progressionEntity.setSegmentation(1);
        ctx.getQueueRef().send("update-state", state);
        System.out.println("Sentence segmentation done");

        if(Objects.isNull(ctx.getReasonExtractorStrategy()) && Objects.isNull(ctx.getEmotionExtractionStrategy())) {
            state.pe.setRaisons(-1);
            state.pe.setEmotions(-1);
            ctx.getQueueRef().send("update-state", state);
        } else {
            ctx.getQueueRef().send("extract-phrases", state);
        }

        if(Objects.isNull(ctx.getFactExtractorStrategy())) {
            state.pe.setFaits(-1);
            ctx.getQueueRef().send("update-state", state);
        } else {
            ctx.getQueueRef().send("extract-facts", state);
        }

        if(Objects.isNull(ctx.getParticipantExtractionStrategy())) {
            state.pe.setParticipants(-1);
            ctx.getQueueRef().send("update-state", state);
        } else {
            ctx.getQueueRef().send("extract-participants", state);
        }
    }
}
