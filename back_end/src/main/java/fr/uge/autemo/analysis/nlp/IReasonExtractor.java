package fr.uge.autemo.analysis.nlp;

import fr.uge.autemo.analysis.pojo.Phrase;
import fr.uge.autemo.analysis.pojo.Raison;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;

@FunctionalInterface
public interface IReasonExtractor {
    ArrayList<Raison> execute(String scene, Phrase phrase) throws IOException, InterruptedException, ParseException;
}
