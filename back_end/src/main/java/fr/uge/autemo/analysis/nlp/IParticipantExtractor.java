package fr.uge.autemo.analysis.nlp;

import com.google.gson.internal.LinkedTreeMap;
import fr.uge.autemo.analysis.pojo.Participant;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;

@FunctionalInterface
public interface IParticipantExtractor {
    ArrayList<Participant> execute(ArrayList<LinkedTreeMap<?, ?>> arrayList) throws IOException, InterruptedException, ParseException;
}

