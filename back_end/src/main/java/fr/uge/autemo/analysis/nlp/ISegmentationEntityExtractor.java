package fr.uge.autemo.analysis.nlp;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;

/**
 * Interfacce for classes that realize segmentation and participant extraction at the same time,
 * from the same python script for example
 */
public interface ISegmentationEntityExtractor {
    
    //TODO documentation
    
    /**
     *
     * @param sceneText
     * @return A hashmap that contains the segmentation and the participants from a scene. The keys are "segmentation" and "entities"
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     */
    HashMap<String, String> execute(String sceneText) throws IOException, InterruptedException, ParseException;
}
