/*
{
   "scène": string,
   "id":int,
   "participants":[
     { [key:string]: {
         "qualificateurs":[ { [key:string] : string} ],
         "roles":string
     }}
   ],
   "phrases":[
     { [key:string]:{
         "texte":string,
         "émotion":"colère/peur/tristesse/joie",
         "raisons":[
           {
             [key:string]:{ "debut": int, "fin": int }
           }
         ]
     }}
   ],
   "error":null
 }
 */
package fr.uge.autemo.analysis.pojo.payload;

import java.util.Objects;

public class AnalysisPayload {

    public String scene;
    public Long id;
    public double threshold;
    public String error;
    public String titre;

    public AnalysisPayload() {
    }

    public AnalysisPayload(String titre, String scene, Long id, double threshold, String error) {
        Objects.requireNonNull(scene);
        if (id < 0){
            throw new IllegalArgumentException("AnalysisPayload: id cannot be negative");
        }
        if(threshold>=1 || threshold<=0) {
            threshold = 0.3;
        }
        System.out.println("---------------THRESHOLD IN ANALYSIS PAYLOAD---------------");
        System.out.println(threshold);
        this.threshold = threshold;
        this.titre = titre;
        this.scene = scene;
        this.id = id;
        this.error = error;
    }
}
