package fr.uge.autemo.analysis.entities;

import javax.persistence.*;

@Entity
@Table(name = "raison")
public class RaisonEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_raison")
    private Long idRaison;

    @Column(name = "id_phrase")
    private Long idPhrase;

    @Column(name = "debut")
    private int debut;

    @Column(name = "fin")
    private int fin;

    public Long getIdRaison() {
        return this.idRaison;
    }

    public Long getIdPhrase() {
        return this.idPhrase;
    }

    public void setIdPhrase(Long idPhrase) {
        this.idPhrase = idPhrase;
    }

    public int getDebut() {
        return this.debut;
    }

    public void setDebut(int debut) {
        this.debut = debut;
    }

    public int getFin() {
        return this.fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "RaisonEntity{" +
                "idRaison=" + idRaison +
                ", idPhrase=" + idPhrase +
                ", debut=" + debut +
                ", fin=" + fin +
                '}';
    }
}
