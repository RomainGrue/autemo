package fr.uge.autemo.analysis.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "participant")
public class ParticipantEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_participant")
    private Long idParticipant;

    @Column(name = "id_scene")
    private Long idScene;

    @Column(name = "role")
    private String role;

    @OneToMany //(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_participant")
    private List<QualificateurEntity> qualificateurEntityList;

    public List<QualificateurEntity> getQualificateurEntityList() {
        return qualificateurEntityList;
    }

    public void setQualificateurEntityList(List<QualificateurEntity> qualificateurEntityList) {
        this.qualificateurEntityList = qualificateurEntityList;
    }

    public Long getIdParticipant() {
        return this.idParticipant;
    }

    public void setIdParticipant(Long idParticipant) {
        this.idParticipant = idParticipant;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "ParticipantEntity{" +
                "idParticipant=" + idParticipant +
                ", idScene=" + idScene +
                ", role='" + role + '\'' +
                '}';
    }
}
