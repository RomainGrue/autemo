package fr.uge.autemo.analysis.nlp.participantextractors;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import fr.uge.autemo.analysis.nlp.IParticipantExtractor;
import fr.uge.autemo.analysis.pojo.Participant;
import fr.uge.autemo.analysis.pojo.Qualificateur;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class with a static method to get the participants of a scene
 */
public class NERSpacyParticipantExtracteur implements IParticipantExtractor {

    /**
     * Call a python script to get all the participants.
     * Return a list of participants. A participant is a qualifier and a role. A qualifier contains the qualifier,
     * a start index and an ending index
     * @return ArrayList of participants of the scene
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     */
    @Override
    public ArrayList<Participant> execute(ArrayList<LinkedTreeMap<?, ?>> s) throws IOException, InterruptedException, ParseException {
        Gson gson = new Gson();

        ArrayList<Participant> ret = new ArrayList<>();
        s.forEach(e -> {
            ArrayList<Qualificateur> temp = new ArrayList<>();
            var qualif = new Qualificateur(
                    null,
                    ((Double) e.get("start_char")).intValue(),
                    ((Double) e.get("end_char")).intValue()
            );
            if((e.get("type")).equals("PER")) {
                temp.add(qualif);
            }
            if(!temp.isEmpty()) {
                ret.add(new Participant(
                        null,
                        temp,
                        null
                ));
            }
        });
        return ret;
    }

}
