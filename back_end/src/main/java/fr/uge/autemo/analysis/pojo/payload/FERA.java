package fr.uge.autemo.analysis.pojo.payload;

import fr.uge.autemo.analysis.pojo.Action;
import fr.uge.autemo.analysis.pojo.Fait;
import fr.uge.autemo.analysis.pojo.Participant;
import fr.uge.autemo.analysis.pojo.Phrase;

import java.util.List;

public class FERA {
    public Long id;
    public double threshold;
    public List<Participant> participants;
    public List<Phrase> phrases;
    public List<Fait> faits;
    public List<Action> actions;
    public String error;
    public String text;

    public FERA(Long id,
                double threshold,
                List<Participant> participants,
                List<Phrase> phrases,
                List<Fait> faits,
                List<Action> actions,
                String text,
                String error) {
        this.id = id;
        this.threshold = threshold;
        this.participants = participants;
        this.phrases = phrases;
        this.faits = faits;
        this.actions = actions;
        this.error = error;
        this.text = text;
    }
}
