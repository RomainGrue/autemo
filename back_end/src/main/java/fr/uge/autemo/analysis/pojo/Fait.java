package fr.uge.autemo.analysis.pojo;

public class Fait {
    public Long id;
    public boolean interieur;
    public int debut;
    public int fin;

    public Fait(Long id, boolean interieur, int debut, int fin) {
        this.interieur = interieur;
        this.debut = debut;
        this.fin = fin;
        this.id = id;
    }
}
