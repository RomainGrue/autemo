package fr.uge.autemo.analysis.pojo;

import java.util.Objects;

/**
 */
public final class Qualificateur {
    public Long id;
    public int debut;
    public int fin;

    /**
     * @param debut start index
     * @param fin end index
     */
    public Qualificateur(Long id, int debut, int fin) {
        this.debut = debut;
        this.fin = fin;
        this.id = id;
    }



}
