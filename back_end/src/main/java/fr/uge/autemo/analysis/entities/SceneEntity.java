package fr.uge.autemo.analysis.entities;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@Table(name = "scene")
public class SceneEntity /*extends PanacheEntity*/ {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_scene")
    private Long idScene;

    @Column(name = "corrected")
    private Boolean corrected;

    @FullTextField(analyzer = "french")
    @Column(name = "text")
    private String text;

    @FullTextField(analyzer = "french")
    @Column(name = "titre")
    private String titre;

    @Column(name = "threshold")
    private double threshold;

    @OneToMany
    @JoinColumn(name = "id_scene")
    private List<PhraseEntity> phraseEntityList;

    @OneToMany
    @JoinColumn(name = "id_scene")
    private List<ParticipantEntity> participantEntityList;

    @OneToMany
    @JoinColumn(name = "id_scene")
    private List<FaitEntity> faitEntityList;

    @OneToMany
    @JoinColumn(name = "id_scene")
    private List<ActionEntity> actionEntityList;

    public void setFaitEntityList(List<FaitEntity> faitEntityList) {
        this.faitEntityList = faitEntityList;
    }

    public List<ActionEntity> getActionEntityList() {
        return actionEntityList;
    }

    public void setActionEntityList(List<ActionEntity> actionEntityList) {
        this.actionEntityList = actionEntityList;
    }

    public List<FaitEntity> getFaitEntityList() {
        return faitEntityList;
    }

    public List<ParticipantEntity> getParticipantEntityList() {
        return participantEntityList;
    }

    public void setParticipantEntityList(List<ParticipantEntity> participantEntityList) {
        this.participantEntityList = participantEntityList;
    }

    public List<PhraseEntity> getPhraseEntityList() {
        return phraseEntityList;
    }

    public void setPhraseEntityList(List<PhraseEntity> phraseEntityList) {
        this.phraseEntityList = phraseEntityList;
    }

    public Long getIdScene() {
        return this.idScene;
    }

    public void setIdScene(Long idScene) {
        this.idScene = idScene;
    }

    public Boolean getCorrected() {
        return this.corrected;
    }

    public void setCorrected(Boolean corrected) {
        this.corrected = corrected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    /**
     * Check if the title exists already in the db
     * @param title title to check
     * @param em EntityManager
     * @return True if the title exist
     */
    static public Boolean titleExist(String title, EntityManager em){
        var listOfTitle = em.createNativeQuery("SELECT 1 FROM scene WHERE titre = :title")
                .setParameter("title", title)
                .getResultList();
        return !listOfTitle.isEmpty();
    }
}
