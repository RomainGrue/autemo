package fr.uge.autemo.analysis.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.util.List;
import java.util.Objects;

public final class Phrase {

    public Long id;
    public int emotion;
    public List<Raison> raison;
    public int debut;
    public int fin;

    /**
     */
    public Phrase(Long id, int debut, int fin, int emotion, List<Raison> raison) {
        this.debut = debut;
        this.fin = fin;
        this.emotion = emotion;
        this.raison = raison;
        this.id = id;
    }
}
