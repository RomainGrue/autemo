package fr.uge.autemo.analysis.pojo;

import java.util.Objects;

/**
 */
public final class Raison {
    public Long id;
    public int debut;
    public int fin;

    /**
     * @param debut start index of the reason
     * @param fin end index of the reason
     */
    public Raison(Long id, int debut, int fin) {
        this.id = id;
        this.debut = debut;
        this.fin = fin;
    }

    public int debut() {
        return debut;
    }

    public int fin() {
        return fin;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Raison) obj;
        return this.debut == that.debut &&
                this.fin == that.fin;
    }

    @Override
    public int hashCode() {
        return Objects.hash(debut, fin);
    }

    @Override
    public String toString() {
        return "Raison[" +
                "debut=" + debut + ", " +
                "fin=" + fin + ']';
    }

}
