package fr.uge.autemo.correction;

public class PossibleSavingError {

    public static PossibleSavingError NO_ERROR = new PossibleSavingError(false, "");

    private final boolean errorOccur;
    private final String errorMessage;

    private PossibleSavingError(boolean errorOccur, String errorMessage) {
        this.errorOccur = errorOccur;
        this.errorMessage = errorMessage;
    }

    public PossibleSavingError(String errorMessage) {
        this(false, errorMessage);
    }


    public boolean hasOccured() {
        return errorOccur;
    }

    public String errorMessage() {
        return errorMessage;
    }
}