package fr.uge.autemo.correction;

import fr.uge.autemo.analysis.entities.*;
import fr.uge.autemo.analysis.pojo.*;
import fr.uge.autemo.analysis.pojo.payload.FERA;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.Collections;

@Path("/API/persist")
public class CorrectionRessource {
    @Inject
    EntityManager entityManager;

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public FERA get(FERA fera) {
        SceneEntity scene = entityManager.find(SceneEntity.class, fera.id);

        if (scene == null) {
            return generateErrorFera(fera.id, "Scene not found: id " + fera.id + " does not exist");
        }

        var possibleError = processScene(fera, scene, entityManager);
        if (possibleError.isPresent()) {
            return possibleError.get();
        }
        entityManager.flush();

        return fera;
    }

    private static FERA generateErrorFera(Long id, String errorMessage) {
        return new FERA(
                id,
                -1,
                Collections.<Participant>emptyList(),
                Collections.<Phrase>emptyList(),
                Collections.<Fait>emptyList(),
                Collections.<Action>emptyList(),
                "",
                errorMessage
        );
    }

    private static Optional<FERA> processScene(FERA fera, SceneEntity scene, EntityManager entityManager) {
        var error = processParticipants(fera.participants, scene, fera.id, entityManager);
        if (error.isPresent()) {
            return error;
        }

        error = processPhrases(fera.phrases, scene, fera.id, entityManager);
        if (error.isPresent()) {
            return error;
        }

        error = processFaits(fera.faits, scene, fera.id, entityManager);
        if (error.isPresent()) {
            return error;
        }

        error = processActions(fera.actions, scene, fera.id, entityManager);
        return error;
    }

    private static Optional<FERA> processParticipants(List<Participant> participants, SceneEntity scene, long feraId, EntityManager entityManager) {
        var listParticipantsIdFromPayload = new ArrayList<Long>();
        var listQualificateursIdFromPayload = new ArrayList<Long>();

        var possibleError = saveParticipants(participants, feraId, entityManager, listParticipantsIdFromPayload, listQualificateursIdFromPayload);
        if (possibleError.hasOccured()) {
            return Optional.of(generateErrorFera(feraId, possibleError.errorMessage()));
        }

        deleteUnusedQualificateurs(scene, listQualificateursIdFromPayload, entityManager);
        deleteUnusedParticipants(scene, listParticipantsIdFromPayload, entityManager);

        return Optional.empty();
    }

    private static Optional<FERA> processPhrases(List<Phrase> phrases, SceneEntity scene, long feraId, EntityManager entityManager) {
        var listRaisonsIdFromPayload = new ArrayList<Long>();
        var listPhrasesIdFromPayload = new ArrayList<Long>();

        var possibleError = savePhrases(phrases, scene.getIdScene(), entityManager, listPhrasesIdFromPayload, listRaisonsIdFromPayload);
        if (possibleError.hasOccured()) {
            return Optional.of(generateErrorFera(feraId, possibleError.errorMessage()));
        }

        deleteUnusedRaisons(scene, listRaisonsIdFromPayload, entityManager);
        deleteUnusedPhrases(scene, listPhrasesIdFromPayload, entityManager);

        return Optional.empty();
    }

    private static Optional<FERA> processFaits(List<Fait> faits, SceneEntity scene, long feraId, EntityManager entityManager) {
        var listFaitsIdFromPayload = new ArrayList<Long>();

        var possibleError = saveFaits(feraId, faits, entityManager, listFaitsIdFromPayload);
        if (possibleError.hasOccured()) {
            return Optional.of(generateErrorFera(feraId, possibleError.errorMessage()));
        }

        deleteUnusedFaits(scene, listFaitsIdFromPayload, entityManager);

        return Optional.empty();
    }

    private static Optional<FERA> processActions(List<Action> actions, SceneEntity scene, Long feraId, EntityManager entityManager) {
        var listActionsIdFromPayload = new ArrayList<Long>();

        var possibleError = saveActions(actions, feraId, entityManager, listActionsIdFromPayload);
        if (possibleError.hasOccured()) {
            return Optional.of(generateErrorFera(feraId, possibleError.errorMessage()));
        }

        deleteUnusedActions(scene, listActionsIdFromPayload, entityManager);

        return Optional.empty();
    }



    private static PossibleSavingError saveParticipants(List<Participant> participants, long sceneId, EntityManager entityManager, List<Long> listParticipantsIdFromPayload, List<Long> listQualificateursIdFromPayload) {
        for (var participant : participants) {
            var participantEntity = new ParticipantEntity();
            if (participant.id != null) {
                participantEntity = entityManager.find(ParticipantEntity.class, participant.id);
                if (participantEntity == null) {
                    return new PossibleSavingError("Participant not found: id " + participant.id + " does not exist");
                }
            } else {
                participantEntity.setIdScene(sceneId);
            }

            saveParticipant(participantEntity, participant, sceneId, entityManager);
            listParticipantsIdFromPayload.add(participantEntity.getIdParticipant());

            var possibleError = saveQualificateurs(participant.qualificateur, participantEntity.getIdParticipant(), listQualificateursIdFromPayload, entityManager);
            if (possibleError.hasOccured()) {
                return possibleError;
            }
        }
        return PossibleSavingError.NO_ERROR;
    }

    private static PossibleSavingError saveQualificateurs(List<Qualificateur> qualificateurs, long participantId, List<Long> listQualificateursIdFromPayload, EntityManager entityManager) {
        for (var qualificateur : qualificateurs) {
            var qualificateurEntity = new QualificateurEntity();
            if (qualificateur.id != null) {
                qualificateurEntity = entityManager.find(QualificateurEntity.class, qualificateur.id);
                if (qualificateurEntity == null) {
                    return new PossibleSavingError("Qualificateur not found: id " + qualificateur.id + " does not exist");
                }
            } else {
                qualificateurEntity.setIdParticipant(participantId);
            }

            saveQualificateur(qualificateurEntity, qualificateur, participantId, entityManager);
            listQualificateursIdFromPayload.add(qualificateurEntity.getIdQualificateur());
        }
        return PossibleSavingError.NO_ERROR;
    }

    private static PossibleSavingError savePhrases(List<Phrase> phrases, long sceneId, EntityManager entityManager, List<Long> listPhrasesIdFromPayload, List<Long> listRaisonsIdFromPayload) {
        for (var phrase : phrases) {
            if (phrase.id == null) {
                return new PossibleSavingError("Impossible to add phrase to a saved scene");
            } else {
                var phraseEntity = entityManager.find(PhraseEntity.class, phrase.id);
                if (phraseEntity == null) {
                    return new PossibleSavingError("Phrase not found: id " + phrase.id + " does not exist");
                }

                savePhrase(phraseEntity, phrase, sceneId, entityManager);
                listPhrasesIdFromPayload.add(phraseEntity.getIdPhrase());

                if (phrase.raison != null) {
                    var possibleError = saveRaisons(phrase.raison, phrase, entityManager, listRaisonsIdFromPayload);
                    if (possibleError.hasOccured()) {
                        return possibleError;
                    }
                }
            }
        }
        return PossibleSavingError.NO_ERROR;
    }

    private static PossibleSavingError saveRaisons(List<Raison> raisons, Phrase phrase, EntityManager entityManager, List<Long> listRaisonsIdFromPayload) {
        for (var raison : raisons) {
            var raisonEntity = new RaisonEntity();
            if (raison.id != null) {
                raisonEntity = entityManager.find(RaisonEntity.class, raison.id);
                if (raisonEntity == null) {
                    return new PossibleSavingError("Raison not found: id " + raison.id + " does not exist");
                }
            }

            saveRaison(raisonEntity, raison, phrase, entityManager);
            listRaisonsIdFromPayload.add(raisonEntity.getIdRaison());
        }
        return PossibleSavingError.NO_ERROR;
    }

    private static PossibleSavingError saveFaits(long sceneId, List<Fait> faits, EntityManager entityManager, List<Long> listFaitsIdFromPayload) {
        for (var fait : faits) {
            var faitEntity = new FaitEntity();
            if (fait.id != null) {
                faitEntity = entityManager.find(FaitEntity.class, fait.id);
                if (faitEntity == null) {
                    return new PossibleSavingError("Fait not found: id " + fait.id + " does not exist");
                }
            }
            saveFait(faitEntity, fait, sceneId, entityManager);

            listFaitsIdFromPayload.add(faitEntity.getIdFait());
        }
        return PossibleSavingError.NO_ERROR;
    }

    private static PossibleSavingError saveActions(List<Action> actions, long sceneId,  EntityManager entityManager, List<Long> listActionsIdFromPayload) {
        for (var action : actions) {
            var actionEntity = new ActionEntity();
            if (action.id != null && entityManager.find(ActionEntity.class, action.id) == null) {
                actionEntity = entityManager.find(ActionEntity.class, action.id);
                if (actionEntity == null) {
                    return new PossibleSavingError("Action not found: id " + action.id + " does not exist");
                }
            }
            saveAction(actionEntity, action, entityManager, sceneId);
            listActionsIdFromPayload.add(actionEntity.getIdAction());
        }
        return PossibleSavingError.NO_ERROR;
    }


    private static void saveParticipant(ParticipantEntity participantEntity, Participant participant, long sceneId, EntityManager entityManager) {
        participantEntity.setRole(participant.role);
        entityManager.persist(participantEntity);
    }

    private static void saveQualificateur(QualificateurEntity qualificateurEntity, Qualificateur qualificateur, long participantId, EntityManager entityManager) {
        qualificateurEntity.setIdParticipant(participantId);
        qualificateurEntity.setDebut(qualificateur.debut);
        qualificateurEntity.setFin(qualificateur.fin);
        entityManager.persist(qualificateurEntity);
    }

    private static void savePhrase(PhraseEntity phraseEntity, Phrase phrase, long sceneId, EntityManager entityManager) {
        phraseEntity.setIdPhrase(phrase.id);
        phraseEntity.setEmotion(phrase.emotion);
        phraseEntity.setDebut(phrase.debut);
        phraseEntity.setFin(phrase.fin);
        entityManager.persist(phraseEntity);
    }

    private static void saveRaison(RaisonEntity raisonEntity, Raison raison, Phrase phrase, EntityManager entityManager) {
        raisonEntity.setIdPhrase(phrase.id);
        raisonEntity.setDebut(raison.debut);
        raisonEntity.setFin(raison.fin);
        entityManager.persist(raisonEntity);
    }

    private static void saveFait(FaitEntity faitEntity, Fait fait, long sceneId, EntityManager entityManager) {
        faitEntity.setIdFait(fait.id);
        faitEntity.setDebut(fait.debut);
        faitEntity.setFin(fait.fin);
        faitEntity.setInterieur(fait.interieur);
        faitEntity.setIdScene(sceneId);
        entityManager.persist(faitEntity);
    }

    private static void saveAction(ActionEntity actionEntity, Action action, EntityManager entityManager, Long id) {
        actionEntity.setIdAction(action.id);
        actionEntity.setIdScene(id);
        actionEntity.setText(action.text);
        actionEntity.setEstInterieur(action.estinterieur);
        entityManager.persist(actionEntity);
    }


    private static void deleteUnusedQualificateurs(SceneEntity scene, List<Long> listQualificateursIdFromPayload, EntityManager entityManager) {
        scene
                .getParticipantEntityList()
                .stream()
                .flatMap(
                        participantEntity ->
                                participantEntity.getQualificateurEntityList() == null ?
                                        Stream.empty() :
                                        participantEntity.getQualificateurEntityList().stream()
                )
                .filter(
                        qualificateurEntity ->
                                !listQualificateursIdFromPayload.contains(qualificateurEntity.getIdQualificateur())
                )
                .forEach(qualificateurEntity -> {
                            entityManager.remove(
                                    entityManager.find(
                                            QualificateurEntity.class,
                                            qualificateurEntity.getIdQualificateur()
                                    )
                            );
                        }
                );
        entityManager.flush();
    }

    private static void deleteUnusedParticipants(SceneEntity scene, List<Long> listParticipantsIdFromPayload, EntityManager entityManager) {
        scene
                .getParticipantEntityList()
                .stream()
                .filter(
                        participantEntity ->
                                !listParticipantsIdFromPayload.contains(participantEntity.getIdParticipant())
                )
                .forEach(
                        participantEntity ->
                                entityManager.remove(
                                        entityManager.find(
                                                ParticipantEntity.class,
                                                participantEntity.getIdParticipant()
                                        )
                                )
                );
    }

    private static void deleteUnusedPhrases(SceneEntity scene, List<Long> listPhrasesIdFromPayload, EntityManager entityManager) {
        scene
                .getPhraseEntityList()
                .stream()
                .filter(phraseEntity -> !listPhrasesIdFromPayload.contains(phraseEntity.getIdPhrase()))
                .forEach(phraseEntity ->
                        entityManager.remove(entityManager.find(PhraseEntity.class, phraseEntity.getIdPhrase()))
                );
    }

    private static void deleteUnusedRaisons(SceneEntity scene, List<Long> listRaisonsIdFromPayload, EntityManager entityManager) {
        scene
                .getPhraseEntityList()
                .stream()
                .flatMap(phraseEntity ->
                        phraseEntity.getRaisonEntityList() == null ?
                                Stream.empty() :
                                phraseEntity.getRaisonEntityList().stream()
                )
                .filter(raisonEntity -> !listRaisonsIdFromPayload.contains(raisonEntity.getIdRaison()))
                .forEach(raisonEntity ->
                        entityManager.remove(entityManager.find(RaisonEntity.class, raisonEntity.getIdRaison()))
                );
    }

    private static void deleteUnusedFaits(SceneEntity scene, List<Long> listFaitsIdFromPayload, EntityManager entityManager) {
        scene.getFaitEntityList()
                .stream()
                .filter(faitEntity -> !listFaitsIdFromPayload.contains(faitEntity.getIdFait()))
                .forEach(faitEntity ->
                        entityManager.remove(entityManager.find(FaitEntity.class, faitEntity.getIdFait()))
                );
    }

    private static void deleteUnusedActions(SceneEntity scene, List<Long> listActionsIdFromPayload, EntityManager entityManager) {
        scene
                .getActionEntityList()
                .stream()
                .filter(actionEntity -> !listActionsIdFromPayload.contains(actionEntity.getIdAction()))
                .forEach(actionEntity ->
                        entityManager.remove(
                                entityManager.find(ActionEntity.class, actionEntity.getIdAction())
                        )
                );
    }
}
