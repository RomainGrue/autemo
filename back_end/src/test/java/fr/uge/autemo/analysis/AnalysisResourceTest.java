package fr.uge.autemo.analysis;

import fr.uge.autemo.analysis.pojo.body.SceneText;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import io.quarkus.test.junit.QuarkusTest;

import static org.hamcrest.CoreMatchers.is;

import static io.restassured.RestAssured.given;

@QuarkusTest
class AnalysisResourceTest {

    // Full analysis

    /*@Test
    @Tag("Should analyse a scene correctly")
    void fullAnalysis() throws InterruptedException {
        var text = "Hier je suis allé acheter du pain avec Romain. Je suis content et Leo aussi.";
        var extractedBody = given()
                .contentType("application/json")
                .body(new SceneText(text))
                .when()
                .post("/API/analyse")
                .then()
                .extract();

        int id = extractedBody.path("id");

        boolean emotionState = false;
        boolean segmentationState = false;
        boolean participantState = false;
        while (!emotionState || !segmentationState || !participantState) {
            System.out.println("////////////////////");
            System.out.println("current state : ");
            System.out.println(emotionState);
            System.out.println(segmentationState);
            System.out.println(participantState);
            
            var body = given()
                    .when()
                    .get("/API/analysis-state/" + id)
                    .then()
                    .extract();
            emotionState = body.path("emotions");
            segmentationState = body.path("segmentation");
            participantState = body.path("participants");
            Thread.sleep(5000);
        }

        var analysis = given()
                .when().get("/API/analyse/" + id)
                .then()
                .statusCode(200)
                .body("participants.size()", is(2))
                .extract();

    }

     */



    // GET ROUTE
//    @Test
//    @Tag("Should return a FERA grid given an existing id")
//    void getFromID() {
//        given()
//                .when().get("/API/analyse/63")
//                .then()
//                .statusCode(200)
//                .body("participants.size()", is(1));
//    }
//
//    @Test
//    @Tag("Should return an error given an unknown ID")
//    void getFromIncorrectID() {
//        given()
//                .when().get("/API/analyse/100")
//                .then()
//                .statusCode(200)
//                .body("error", is("Not found: id does not exist"));
//    }
//
//    @Test
//    @Tag("Should return error if body is malformed")
//    void getWithoutRequirements() {
//        given()
//                .when().get("/API/analyse/-1")
//                .then()
//                .statusCode(200)
//                .body("error", is("Malformed body: id cannot be negative"));
//    }
//
//    // POST ROUTE
//
//    @Test
//    @Tag("Should return an id")
//    void postShouldReturnCorrectID() {
//        var text = "Mon texte.";
//        given()
//                .contentType("application/json")
//                .body(new SceneText(text))
//                .when()
//                .post("/API/analyse")
//                .then()
//                .statusCode(200)
//                .body("scene", is(text));
//
//    }
//
//    @Test
//    @Tag("Should return an error if body is malformed")
//    void postShouldReturnError() {
//        given()
//                .contentType("application/json")
//                .body(new SceneText(""))
//                .when()
//                .post("/API/analyse")
//                .then()
//                .statusCode(500);
//    }

}