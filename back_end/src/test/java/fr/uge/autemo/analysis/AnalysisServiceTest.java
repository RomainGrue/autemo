package fr.uge.autemo.analysis;

import fr.uge.autemo.analysis.entities.SceneEntity;
import fr.uge.autemo.analysis.nlp.AnalysisContext;
import fr.uge.autemo.analysis.pojo.*;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AnalysisServiceTest {
    @Inject
    EntityManager em;
    @Inject
    EventBus bus;

    private Long id;

    /*@Order(1)
    @Test
    @DisplayName("AnalysisService#analyse#Should insert -1 in the whole progression")
    @Transactional
    void insertErrorIfSegFailure() throws IOException {
        var se = new SceneEntity();
        se.setCorrected(false);
        em.persist(se);
        em.flush();
        var ctx = new AnalysisContext.AnalysisContextBuilder()
                .setId(se.getIdScene())
                .setScene("this is my scene")
                .setSegmentationStrategy(s -> {throw new IOException();})
                .setEm(em)
                .setQueueRef(bus)
                .build();
        var service = new AnalysisService();
        service.analyse(ctx);
        this.id = se.getIdScene();

    }

    @Order(2)
    @Test
    @DisplayName("AnalysisService#analyse#Should insert -1 in the whole progression")
    @Transactional
    void insertErrorIfSegFailure2() {
        System.out.println(id);
        given()
                .when().get("/API/analysis-state/" + this.id)
                .then()
                .statusCode(200)
                .body("segmentation", is(-1))
                .body("faits", is(-1))
                .body("emotions", is(-1))
                .body("raisons", is(-1))
                .body("participants", is(-1))
                .body("error", is(IsNull.nullValue()));
    }

    @Order(3)
    @Test
    @DisplayName("AnalysisService#all#Should insert 1 in the whole progression")
    @Transactional
    void insertOneIfItDoesntFail() throws IOException, InterruptedException {
        var se = new SceneEntity();
        se.setCorrected(false);
        em.persist(se);
        em.flush();
        this.id = se.getIdScene();
        Thread.sleep(1500); //fixme

        var reason = new Raison(-1L, 42, 43);
        var fact = new Fait(-1L, true, 42L,43L);
        var emotion = new Phrase(-1L, "First sentence", 0, List.of(reason));
        var qualif = new Qualificateur(-1L, 0, 6);
        var participant = new Participant(-1L, List.of(qualif), "role");

        var ctx = new AnalysisContext.AnalysisContextBuilder()
                .setId(this.id)
                .setScene("First sentence. Second sentence.")
                .setSegmentationStrategy(s -> new ArrayList<>(List.of("First sentence.", "Second sentence")))
                .setReasonExtractorStrategy((scene, phrase) -> new ArrayList<>(List.of(reason)))
                .setFactExtractorStrategy(() -> new ArrayList<>(List.of(fact)))
                .setEmotionExtractionStrategy(l -> new ArrayList<>(List.of(emotion)))
                .setParticipantExtractionStrategy(linkedTreeMaps -> new ArrayList<>(List.of(participant)))
                .setEm(em)
                .setQueueRef(bus)
                .build();
        var service = new AnalysisService();
        service.analyse(ctx);
        Thread.sleep(1500); //fixme
    }

    @Order(4)
    @Test
    @DisplayName("AnalysisService#all#Should insert 1 in the whole progression")
    @Transactional
    void insertOneIfItDoesntFail2() {
        System.out.println(id);
        given()
                .when().get("/API/analysis-state/" + this.id)
                .then()
                .statusCode(200)
                .body("segmentation", is(1))
                .body("faits", is(1))
                .body("emotions", is(1))
                .body("raisons", is(1))
                .body("participants", is(1))
                .body("error", is(IsNull.nullValue()));
    }

     */

}